package cz.linhy.clovece.network;

import cz.linhy.clovece.windows.ConnectWindow;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

/**
 * Třída Socket zajiš%tuje příjem a odesílání zpráv. Při ztrátě spojení se pokouší o opětovné připojení.
 */
public class Socket {
    private static Socket instance;
    private PrintWriter pw;
    private java.net.Socket comSocket;

    private String playerName;
    private boolean connected;
    private boolean loggedIn = false;
    private int loginAttempts = 0;
    private int playerId = Integer.MIN_VALUE;

    private final String IP;
    private final int port;

    private String lobbyID;
    private boolean quit;

    private Timer timer;
    private TimerTask task;
    private Thread readThread;

    private List<ChangeListener> listeners = new ArrayList<>();

    private static final String IP_PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";

    /**
     * Konstruktor, ověří zadanou IP adresu a pokud není správná, použije výchozí adresu
     * @param ip IP adresa a port, ke kterému se připojit
     */
    private Socket(String ip) {
        String[] splitString = ip.split(":");

        String IP = "127.0.0.1";
        int port = 9652;
        quit = false;
        if (splitString.length >= 1) {
            if (splitString[0].matches(IP_PATTERN)) {
                IP = splitString[0];
            } else {
                System.out.println("WARNING: Error reading IP address: " + splitString[0] + ", using default IP: 127.0.0.1");
            }

            if (splitString.length == 2) {
                try {
                    int tempPort = Integer.parseInt(splitString[1]);
                    if (tempPort > 0 && tempPort < 65535) {
                        port = tempPort;
                    } else {
                        System.out.println("WARNING: Port " + splitString[1] + " out of range, using default port!");
                    }
                } catch (NumberFormatException e) {
                    System.out.println("WARNING: Error parsing port " + splitString[1] + ", using default port!");
                }
            }
        }

        this.IP = IP;
        this.port = port;

        try {
            comSocket = new java.net.Socket(IP, port);
            connected = true;
            OutputStream os = comSocket.getOutputStream();
            pw = new PrintWriter(os, true);

            readThread = createReadThread();
            readThread.start();

            timer = new Timer("PingPong");
            task = createNewTimerTask();
            timer.scheduleAtFixedRate(task, 0, 7500);
        } catch (IOException e) {
            System.out.println("ERROR: Could not connect to socket!");
            connected = false;
            loggedIn = false;
        }
    }

    /**
     * Zpracovává akce při příjmu zprávy. Zpracuje pouze Login zprávu, ostatní zprávy předá posluchačům
     * @param receivedMessage Přijatá zpráva
     */
    private void onEvent(String receivedMessage) {
        String[] messageSplit = receivedMessage.split(";");
        String command = messageSplit[0];

        if (command.equals("LR")) {
            this.loggedIn = true;
            this.playerId = Integer.parseInt(messageSplit[1]);
        } else {
            ChangeEvent evt = new ChangeEvent(messageSplit);
            for (ChangeListener l : listeners) {
                l.stateChanged(evt);
            }
        }
    }

    /**
     * Přidá instanci posluchače do seznamu posluchačů
     * @param listener instance posluchače
     */
    public void addEventListener(ChangeListener listener) {
        this.listeners.add(listener);
    }

    /**
     * Vrátí aktuální instanci
     * @return instance Socketu
     */
    public static Socket getSocket() {
        return instance;
    }

    /**
     * Vytvoří nové vlákno, které ve smyčce čte zprávy ze socketu a předává funkcím pro zpracování
     * @return vlákno pro čtení zprávy
     */
    private Thread createReadThread() {

        return new Thread(() -> {
            String output;
            InputStream is;
            try {
                while(true) {
                    is = comSocket.getInputStream();
                    byte[] buffer = new byte[100];

                    int readOut = is.read(buffer);
                    if (readOut == -1) {
                        if (quit) {
                            output = "DCD";
                        } else {
                            output = "DC";
                        }

                        if (timer != null) {
                            timer.cancel();
                            timer = null;
                        }

                        quit = false;
                        connected = false;
                        onEvent(output.trim());
                        break;

                    }
                    output = "";
                    for (byte b : buffer) {
                        if (b == 0 || b == 7 /* DING DONG :) */) {
                            break;
                        }
                        output += (char) b;
                    }

                    onEvent(output.trim());
                }
            } catch (IOException e) {
                if (quit) {
                    output = "DCD";
                } else {
                    output = "DC";
                }

                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }

                quit = false;
                connected = false;
                onEvent(output.trim());
            }
        });
    }

    /**
     * Pokusí se odeslat zprávu. Pokud odeslání selže, pokusí se o obnovení připojení a opětovné odeslání.
     * @param message Zpráva k odeslání
     */
    private void sendMessage(String message) {
        if (connected) {
            this.pw.println(message);
        } else {
            System.out.println("ERROR: Cannot send message, not connected to server! Attempting reconnect!");
            try {
                comSocket = new java.net.Socket(IP, port);
                connected = true;

                OutputStream os = comSocket.getOutputStream();
                pw = new PrintWriter(os, true);
                System.out.println("INFO: Connected back to the server!");

                readThread = createReadThread();
                readThread.start();


                timer = new Timer("PingPong");
                task = createNewTimerTask();
                timer.scheduleAtFixedRate(task, 0, 7500);

                this.pw.println(message);
            } catch (IOException e) {
                System.out.println("ERROR: Could not connect to socket!");
                connected = false;
                loggedIn = false;

                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }
            }
        }
    }

    /**
     * Vytvoří Socket s danou IP adresou a portem a uloží instanci
     * @param IP IP adresa a port
     * @return instance Socketu
     */
    public static Socket createSocket(String IP) {
        Socket.instance = new Socket(IP);
        return Socket.instance;
    }

    /**
     * Uloží jméno hráče a odešle přihlašovací zprávu
     * @param playerName jméno hráče
     */
    public void login(String playerName) {
        this.playerName = playerName;
        sendMessage("L;" + playerName);
    }

    /**
     * Pokusí se odelsat zprávu o vytvoření nové hry.
     */
    public void createNewGame() {
        final int[] loginFailCounter = {0};

        Timer timerCreate = new Timer("NewGameTimer");
        TimerTask taskCreate = new TimerTask() {
            public void run() {
                if (loggedIn) {
                    ConnectWindow.connected();
                    pw.println("N;" + playerId);
                    timerCreate.cancel();
                } else {
                    System.out.println("WARNING: Not logged in, skipping register!");

                    loginFailCounter[0]++;
                    if (loginFailCounter[0] == 5) {
                        login(playerName);
                        loginFailCounter[0] = 0;
                        loginAttempts++;
                    }

                    if (loginAttempts == 3) {
                        ConnectWindow.close();
                        System.out.println("ERROR: Server not available!");
                        timerCreate.cancel();
                    }
                }
            }
        };


        timerCreate.scheduleAtFixedRate(taskCreate, 0, 500);
    }

    /**
     * Pokusí se o připojení k lobby se zadaným kódem.
     * @param code kód lobby, ke kterému se Socket připojí
     */
    public void joinGame(String code) {
        final int[] loginFailCounter = {0};
        Timer timerJoin = new Timer("JoinGameTimer");
        TimerTask taskJoin = new TimerTask() {
            public void run() {
                if (loggedIn) {
                    ConnectWindow.connected();
                    pw.println("J;" + playerId + ";" + code);
                    timerJoin.cancel();
                } else {
                    System.out.println("WARNING: Not logged in, skipping join!");

                    loginFailCounter[0]++;
                    if (loginFailCounter[0] == 5) {
                        login(playerName);
                        loginFailCounter[0] = 0;
                        loginAttempts++;
                    }

                    if (loginAttempts == 3) {
                        ConnectWindow.close();
                        System.out.println("ERROR: Server not available!");
                        timerJoin.cancel();
                    }
                }
            }
        };


        timerJoin.scheduleAtFixedRate(taskJoin, 0, 500);
    }

    /**
     * Vrátí jméno hráče
     * @return jméno hráče
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * Odešle odpověď s výběrem barvy
     * @param color číslo barvy k odeslání
     */
    public void sendColor(int color) {
        sendMessage("COR;" + playerId + ";" + color);
    }

    /**
     * Nastaví ID lobby, kte kterému je Socket připojený
     * @param lobbyID ID lobby
     */
    public void setLobbyID(String lobbyID) {
        this.lobbyID = lobbyID;
    }

    /**
     * Odešle zprávu pro spuštění hry
     */
    public void startGame() {
        sendMessage("S;" + lobbyID);
    }

    /**
     * Odešle zprávu o hození kostkou
     */
    public void throwCube() {
        sendMessage("C;" + lobbyID);
    }

    /**
     * Odešle zprávu s ID figurky, která se posouvá
     * @param id ID figurky, která se posouvá
     */
    public void sendMoveReponse(int id) {
        sendMessage("M;" + lobbyID + ";" + playerId + ";" + id);
    }

    /**
     * Odešle zprávu o opuštění hry a uzavře socket
     */
    public void quit() {
        quit = true;
        sendMessage("Q");

        if (timer != null) {
            timer.cancel();
        }

        try {
            comSocket.close();
        } catch (IOException e) {
            System.out.println("ERROR: Error closing socket: " + e.getMessage());
        }
    }

    /**
     * Odešle zprávu Ping
     */
    private void ping() {
        sendMessage("PING");
    }

    /**
     * Odešle žádost o opětovné připojení
     */
    public void reconnect() {
        sendMessage("R;" + playerId);
    }


    /**
     * Vytvoří novou úlohu časovače, který posílá PING zprávy
     * @return úloha časovače pro PING
     */
    private TimerTask createNewTimerTask() {
        return new TimerTask() {
            public void run() {
                Socket.getSocket().ping();
            }
        };
    }
}
