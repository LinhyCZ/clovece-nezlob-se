package cz.linhy.clovece.game;

import cz.linhy.clovece.customComponents.Point;
import cz.linhy.clovece.game.drawer.GameDrawer;
import cz.linhy.clovece.game.gameElements.AllPoints;
import cz.linhy.clovece.game.gameElements.Graphics;
import cz.linhy.clovece.interfaces.IDrawable;
import javafx.scene.paint.Color;

/**
 * Třída zajišťující vykreslení figurky a logiku s figurkami spojenou
 */
public class Figure implements IDrawable {
    private final int id;
    private final Player p;
    private final Color c;
    private final Graphics gr;
    private final double FIGURE_DRAW_OFFSET = 1.6;
    private final double FIGURE_RADIUS = 5;

    private AllPoints point;

    /**
     * Konstruktor figurky
     * @param p Hráč, kterému figurka patří
     * @param id ID figurky
     * @param c Barva figurky
     * @param gr grafická reprezentace figurky
     * @param point bod, na kterém se figurka nachází
     */
    public Figure(Player p, int id, Color c, Graphics gr, AllPoints point) {
        this.p = p;
        this.id = id;
        this.c = c;
        this.gr = gr;
        this.point = point;
    }

    /**
     * Přesune figurku na zadné políčko
     * @param point nové políčko
     */
    public void moveTo(AllPoints point) {
        this.point = point;
    }

    /**
     * Vrátí barvu figurky
     * @return barva figurky
     */
    public Color getColor() {
        return c;
    }

    /**
     * Vrátí bod, na kterém se figurka nachází
     * @return bod, na kterém se figurka nachází
     */
    public AllPoints getPoint() {
        return point;
    }

    /**
     * Vykreslí figurku
     * @param gd kreslítko
     */
    @Override
    public void draw(GameDrawer gd) {
        if (!p.getDisconnected()) {
            gd.drawImageCenter(gr.getGraphicsImage(), new Point(point.getBp().getPoint().getX(), point.getBp().getPoint().getY() + FIGURE_DRAW_OFFSET), 15, 15);
        } else {
            gd.drawImageCenter(Graphics.FIGURE_GR.getGraphicsImage(), new Point(point.getBp().getPoint().getX(), point.getBp().getPoint().getY() + FIGURE_DRAW_OFFSET), 15, 15);
        }

    }

    /**
     * Ověří, zda se figurka neprotíná s danou souřadnicí
     * @param point bod, který se ověřuje
     * @return true = nachází, false = nenachází
     */
    public boolean intersect(Point point) {
        double squareX = Math.pow(point.getX() - this.point.getBp().getPoint().getX(), 2);
        double squareY = Math.pow((point.getY() - this.point.getBp().getPoint().getY() - FIGURE_DRAW_OFFSET), 2);
        double squareRadius = Math.pow(FIGURE_RADIUS, 2);

        return (squareX + squareY < squareRadius);
    }

    /**
     * Vrátí ID figurky
     * @return ID figurky
     */
    public int getId() {
        return id;
    }
}
