package cz.linhy.clovece.game;

import javafx.scene.paint.Color;

/**
 * Enum obsahující všechny typy políček na herním poli
 */
public enum PointType {
    GREEN_START(Color.LIME),
    GREEN__GAME_START(Color.LIME),
    GREEN_HOME(Color.WHITE),

    YELLOW_START(Color.YELLOW),
    YELLOW__GAME_START(Color.YELLOW),
    YELLOW_HOME(Color.WHITE),

    RED_START(Color.RED),
    RED__GAME_START(Color.RED),
    RED_HOME(Color.WHITE),

    BLUE_START(Color.BLUE),
    BLUE__GAME_START(Color.BLUE),
    BLUE_HOME(Color.WHITE),

    GAME(Color.WHITE);

    private Color c;

    /**
     * Konstruktor, vytvoří bod s danou barvou
     * @param c barva bodu
     */
    PointType(Color c) {
        this.c = c;
    }

    /**
     * Vrátí barvu aktuálního bodu
     * @return barva aktuálního bodu
     */
    public Color getColor() {
        return c;
    }
}
