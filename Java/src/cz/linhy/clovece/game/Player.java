package cz.linhy.clovece.game;

import cz.linhy.clovece.game.gameElements.AllPoints;
import cz.linhy.clovece.game.gameElements.Graphics;
import javafx.scene.paint.Color;

/**
 * Třída obsahující informace o hráči
 */
public class Player {
    private final String name;
    private final Color c;

    private boolean disconnected = false;
    private Figure[] figures = new Figure[4];

    /**
     * Konstruktor
     * @param name jméno hráče
     * @param c barva hráče
     */
    public Player(String name, Color c) {
        this.name = name;
        this.c = c;

        initializeFigures();
    }

    /**
     * Přidá hráči figurky jeho barvy
     */
    private void initializeFigures() {
        for (int i = 0; i < 4; i++) {
            if (c.equals(Color.RED)) {
                figures[i] = new Figure(this, i, Color.RED, Graphics.FIGURE_R, AllPoints.values()[i]);
            } else if (c.equals(Color.YELLOW)) {
                figures[i] = new Figure(this, i, Color.YELLOW, Graphics.FIGURE_Y, AllPoints.values()[i + 4]);
            } else if (c.equals(Color.BLUE)) {
                figures[i] = new Figure(this, i, Color.BLUE, Graphics.FIGURE_B, AllPoints.values()[i + 8]);
            } else {
                figures[i] = new Figure(this, i, Color.LIME, Graphics.FIGURE_G, AllPoints.values()[i + 12]);
            }
        }
    }

    /**
     * Vrátí pole s figurkami
     * @return pole figurek
     */
    public Figure[] getFigures() {
        return figures;
    }

    /**
     * Vrátí barvu hráče
     * @return barva hráče
     */
    public Color getColor() {
        return c;
    }

    /**
     * Vrátí jméno hráče
     * @return jméno hráče
     */
    public String getName() {
        return name;
    }

    /**
     * Vrátí, zda je hráč připojený k serveru
     * @return true = je připojený k serveru, false = není připojený k serveru
     */
    public boolean getDisconnected() {
        return disconnected;
    }

    /**
     * Nastaví, zda je hráč připojený k serveru
     * @param disconnected true = je připojený k serveru, false = není připojený k serveru
     */
    public void setDisconnected(boolean disconnected) {
        this.disconnected = disconnected;
    }
}
