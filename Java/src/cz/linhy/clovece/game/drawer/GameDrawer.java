package cz.linhy.clovece.game.drawer;

import cz.linhy.clovece.customComponents.CanvasPane;
import cz.linhy.clovece.customComponents.Point;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;

/**
 * Kreslítko sloužící pro vykreslení různých herních elementů
 */
public class GameDrawer {
    private final double OFFSET = 10.0;
    private final double MAX_SIZE = 150 + 2 * OFFSET;

    private final GraphicsContext gc;
    private final CanvasPane cp;

    /**
     * Konstruktor kreslítka
     * @param cp CanvasPane, na kterou se bude kreslit
     */
    public GameDrawer(CanvasPane cp) {
        this.cp = cp;
        this.gc = cp.getGraphicsContext2D();
    }

    /**
     * Prohodí osu Y (počítání odshora dolů -> počítání odzdola nahoru)
     * @param s souřadnice na ose Y
     * @return opačná souřadnice na ose Y
     */
    public double switchY(double s) {
        return cp.getCanvasSize() - s;
    }

    /**
     * Převede herní souřadnice na reálné souřadnice odpovídající obrazovce
     * @param s herní souřadnice
     * @return reálné souřadnice
     */
    public double fromGameToScreen(double s) {
        double scale = this.cp.getCanvasSize() / this.MAX_SIZE;

        return s * scale;
    }

    /**
     * Převede reálné souřadnice na herní souřadnice
     * @param s reálné souřadnice
     * @return herní souřadnice
     */
    public double fromScreenToGame(double s) {
        double scale = this.MAX_SIZE / this.cp.getCanvasSize();

        return (s * scale) - OFFSET;
    }

    /**
     * Vykreslí ovál v daném bodě
     * @param p Bod
     * @param w Šířka
     * @param h Výška
     */
    public void fillOval(Point p, double w, double h) {
        this.fillOval(p.getX(), p.getY(), w, h);
    }

    /**
     * Vykreslí ovál v daném bodě
     * @param x souřadnice X
     * @param y souřadnice Y
     * @param w Šířka
     * @param h Výška
     */
    public void fillOval(double x, double y, double w, double h) {
        double newW = fromGameToScreen(w);
        double newH = fromGameToScreen(h);

        double newX = fromGameToScreen(x + OFFSET) - (newW / 2);
        double newY = switchY(fromGameToScreen(y + OFFSET)) - (newH / 2);

        gc.fillOval(newX, newY, newW, newH);
    }

    /**
     * Vykreslí obdélník v daném bodě
     * @param x souřadnice X
     * @param y souřadnice Y
     * @param w Šířka
     * @param h Výška
     */
    public void fillRect(double x, double y, double w, double h) {

        double newW = fromGameToScreen(w);
        double newH = fromGameToScreen(h);

        double newX = fromGameToScreen(x + OFFSET);
        double newY = switchY(fromGameToScreen(y + OFFSET));

        gc.fillRect(newX, newY, newW, newH);
    }

    /**
     * Vykreslí obrázek na daném místě. Počítá se od středu a ne od levého horního rohu
     * @param image Obrázek k vykreslení
     * @param p místo, na kterém se vykresluje
     * @param w Šířka
     * @param h Výška
     */
    public void drawImageCenter(Image image, Point p, double w, double h) {
        drawImage(image, p.getX() - w / 2, p.getY() + h / 2, w, h);
    }

    /**
     * Vykreslí obrázek na daném místě.
     * @param image Obrázek k vykreslení
     * @param x souřadnice X
     * @param y souřadnice Y
     * @param w Šířka
     * @param h Výška
     */
    public void drawImage(Image image, double x, double y, double w, double h) {
        double newW = fromGameToScreen(w);
        double newH = fromGameToScreen(h);

        double newX = fromGameToScreen(x + OFFSET);
        double newY = switchY(fromGameToScreen(y + OFFSET));

        gc.drawImage(image, newX, newY, newW, newH);
    }

    /**
     * Nastaví barvu výplně
     * @param paint barva výplně
     */
    public void setFill(Paint paint) {
        gc.setFill(paint);
    }
}
