package cz.linhy.clovece.game.gameElements;

import cz.linhy.clovece.customComponents.Point;
import cz.linhy.clovece.game.PointType;

/**
 * Seznam všech bodů na herní desce
 */
public enum AllPoints {
    POINT_0(new BoardPoint(new Point(0, 15), PointType.RED_START)),
    POINT_1(new BoardPoint(new Point(15, 15), PointType.RED_START)),
    POINT_2(new BoardPoint(new Point(0, 0), PointType.RED_START)),
    POINT_3(new BoardPoint(new Point(15, 0), PointType.RED_START)),
    POINT_4(new BoardPoint(new Point(0, 150), PointType.YELLOW_START)),
    POINT_5(new BoardPoint(new Point(15, 150), PointType.YELLOW_START)),
    POINT_6(new BoardPoint(new Point(0, 135), PointType.YELLOW_START)),
    POINT_7(new BoardPoint(new Point(15, 135), PointType.YELLOW_START)),
    POINT_8(new BoardPoint(new Point(135, 150), PointType.BLUE_START)),
    POINT_9(new BoardPoint(new Point(150, 150), PointType.BLUE_START)),
    POINT_10(new BoardPoint(new Point(135, 135), PointType.BLUE_START)),
    POINT_11(new BoardPoint(new Point(150, 135), PointType.BLUE_START)),
    POINT_12(new BoardPoint(new Point(135, 15), PointType.GREEN_START)),
    POINT_13(new BoardPoint(new Point(150, 15), PointType.GREEN_START)),
    POINT_14(new BoardPoint(new Point(135, 0), PointType.GREEN_START)),
    POINT_15(new BoardPoint(new Point(150, 0), PointType.GREEN_START)),
    POINT_16(new BoardPoint(new Point(60, 0), PointType.RED__GAME_START)),
    POINT_17(new BoardPoint(new Point(60, 15), PointType.GAME)),
    POINT_18(new BoardPoint(new Point(60, 30), PointType.GAME)),
    POINT_19(new BoardPoint(new Point(60, 45), PointType.GAME)),
    POINT_20(new BoardPoint(new Point(60, 60), PointType.GAME)),
    POINT_21(new BoardPoint(new Point(45, 60), PointType.GAME)),
    POINT_22(new BoardPoint(new Point(30, 60), PointType.GAME)),
    POINT_23(new BoardPoint(new Point(15, 60), PointType.GAME)),
    POINT_24(new BoardPoint(new Point(0, 60), PointType.GAME)),
    POINT_25(new BoardPoint(new Point(0, 75), PointType.GAME)),
    POINT_26(new BoardPoint(new Point(0, 90), PointType.YELLOW__GAME_START)),
    POINT_27(new BoardPoint(new Point(15, 90), PointType.GAME)),
    POINT_28(new BoardPoint(new Point(30, 90), PointType.GAME)),
    POINT_29(new BoardPoint(new Point(45, 90), PointType.GAME)),
    POINT_30(new BoardPoint(new Point(60, 90), PointType.GAME)),
    POINT_31(new BoardPoint(new Point(60, 105), PointType.GAME)),
    POINT_32(new BoardPoint(new Point(60, 120), PointType.GAME)),
    POINT_33(new BoardPoint(new Point(60, 135), PointType.GAME)),
    POINT_34(new BoardPoint(new Point(60, 150), PointType.GAME)),
    POINT_35(new BoardPoint(new Point(75, 150), PointType.GAME)),
    POINT_36(new BoardPoint(new Point(90, 150), PointType.BLUE__GAME_START)),
    POINT_37(new BoardPoint(new Point(90, 135), PointType.GAME)),
    POINT_38(new BoardPoint(new Point(90, 120), PointType.GAME)),
    POINT_39(new BoardPoint(new Point(90, 105), PointType.GAME)),
    POINT_40(new BoardPoint(new Point(90, 90), PointType.GAME)),
    POINT_41(new BoardPoint(new Point(105, 90), PointType.GAME)),
    POINT_42(new BoardPoint(new Point(120, 90), PointType.GAME)),
    POINT_43(new BoardPoint(new Point(135, 90), PointType.GAME)),
    POINT_44(new BoardPoint(new Point(150, 90), PointType.GAME)),
    POINT_45(new BoardPoint(new Point(150, 75), PointType.GAME)),
    POINT_46(new BoardPoint(new Point(150, 60), PointType.GREEN__GAME_START)),
    POINT_47(new BoardPoint(new Point(135, 60), PointType.GAME)),
    POINT_48(new BoardPoint(new Point(120, 60), PointType.GAME)),
    POINT_49(new BoardPoint(new Point(105, 60), PointType.GAME)),
    POINT_50(new BoardPoint(new Point(90, 60), PointType.GAME)),
    POINT_51(new BoardPoint(new Point(90, 45), PointType.GAME)),
    POINT_52(new BoardPoint(new Point(90, 30), PointType.GAME)),
    POINT_53(new BoardPoint(new Point(90, 15), PointType.GAME)),
    POINT_54(new BoardPoint(new Point(90, 0), PointType.GAME)),
    POINT_55(new BoardPoint(new Point(75, 0), PointType.GAME)),
    POINT_56(new BoardPoint(new Point(75, 15), PointType.RED_HOME)),
    POINT_57(new BoardPoint(new Point(75, 30), PointType.RED_HOME)),
    POINT_58(new BoardPoint(new Point(75, 45), PointType.RED_HOME)),
    POINT_59(new BoardPoint(new Point(75, 60), PointType.RED_HOME)),
    POINT_60(new BoardPoint(new Point(15, 75), PointType.YELLOW_HOME)),
    POINT_61(new BoardPoint(new Point(30, 75), PointType.YELLOW_HOME)),
    POINT_62(new BoardPoint(new Point(45, 75), PointType.YELLOW_HOME)),
    POINT_63(new BoardPoint(new Point(60, 75), PointType.YELLOW_HOME)),
    POINT_64(new BoardPoint(new Point(75, 135), PointType.BLUE_HOME)),
    POINT_65(new BoardPoint(new Point(75, 120), PointType.BLUE_HOME)),
    POINT_66(new BoardPoint(new Point(75, 105), PointType.BLUE_HOME)),
    POINT_67(new BoardPoint(new Point(75, 90), PointType.BLUE_HOME)),
    POINT_68(new BoardPoint(new Point(135, 75), PointType.GREEN_HOME)),
    POINT_69(new BoardPoint(new Point(120, 75), PointType.GREEN_HOME)),
    POINT_70(new BoardPoint(new Point(105, 75), PointType.GREEN_HOME)),
    POINT_71(new BoardPoint(new Point(90, 75), PointType.GREEN_HOME));

    private BoardPoint bp;

    /**
     * Konstruktor, přidá danému enum herní políčko
     * @param bp herní políčko
     */
    AllPoints(BoardPoint bp) {
        this.bp = bp;
    }

    /**
     * Vrátí herní políčko
     * @return herní políčko
     */
    public BoardPoint getBp() {
        return bp;
    }
}
