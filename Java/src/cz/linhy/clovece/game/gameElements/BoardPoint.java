package cz.linhy.clovece.game.gameElements;

import cz.linhy.clovece.customComponents.Point;
import cz.linhy.clovece.game.PointType;
import cz.linhy.clovece.game.drawer.GameDrawer;
import cz.linhy.clovece.interfaces.IDrawable;
import javafx.scene.paint.Color;

/**
 * Políčko na herní desce, zajišťuje vykreslení
 */
public class BoardPoint implements IDrawable {
    private final Point p;
    private final PointType t;

    private final double WHITE_SIZE = 10;
    private final double BLACK_SIZE = 12;
    private final double COLOR_RING_OFFSET = 3;

    /**
     * Konstruktor políčka
     * @param p místo na desce
     * @param t typ bodu
     */
    public BoardPoint(Point p, PointType t) {
        this.p = p;
        this.t = t;
    }

    /**
     * Zajišťuje vykreslení bodu pomocí kreslítka
     * @param gd kreslítko
     */
    @Override
    public void draw(GameDrawer gd) {
        if (t == PointType.YELLOW__GAME_START || t == PointType.RED__GAME_START || t == PointType.GREEN__GAME_START || t == PointType.BLUE__GAME_START) {
            gd.setFill(Color.BLACK);
            gd.fillOval(p, BLACK_SIZE + COLOR_RING_OFFSET, BLACK_SIZE + COLOR_RING_OFFSET);
            gd.setFill(t.getColor());
            gd.fillOval(p, WHITE_SIZE + COLOR_RING_OFFSET, WHITE_SIZE + COLOR_RING_OFFSET);
            gd.setFill(Color.WHITE);
            gd.fillOval(p, WHITE_SIZE, WHITE_SIZE);
        } else {
            gd.setFill(Color.BLACK);
            gd.fillOval(p, BLACK_SIZE, BLACK_SIZE);
            gd.setFill(t.getColor());
            gd.fillOval(p, WHITE_SIZE, WHITE_SIZE);
        }
    }

    /**
     * Vrátí instanci bodu
     * @return instance bodu
     */
    public Point getPoint() {
        return p;
    }

}
