package cz.linhy.clovece.game.gameElements;

import javafx.scene.image.Image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Enum obsahující grafické reprezentace daných objektů
 */
public enum Graphics {
    DICE_1 ("img/dice_1.png"),
    DICE_2 ("img/dice_2.png"),
    DICE_3 ("img/dice_3.png"),
    DICE_4 ("img/dice_4.png"),
    DICE_5 ("img/dice_5.png"),
    DICE_6 ("img/dice_6.png"),
    FIGURE_R ("img/fig_red.png"),
    FIGURE_Y ("img/fig_yellow.png"),
    FIGURE_B ("img/fig_blue.png"),
    FIGURE_G ("img/fig_green.png"),
    FIGURE_GR ("img/fig_grey.png");

    private final Image graphicsImage;

    /**
     * Vrátí instanci obrázku daného objektu (Grafické reprezentace)
     * @return instance obrázku
     */
    public Image getGraphicsImage() {
        return graphicsImage;
    }

    /**
     * Konstruktor, načte obrázek ze zadané cesty. Pokusí se i o načtení z adresáře JAR
     * @param imagePath cesta k obrázku
     */
    Graphics(String imagePath) {
        Image img;

        try {
            img = new Image(new FileInputStream(imagePath));
        } catch (FileNotFoundException e) {
            if (getClass().getResource("/" + imagePath) != null) {
                img = new Image(getClass().getResource("/" + imagePath).toString());
            } else {
                img = null;
                System.out.print("ERROR: Could not find path: " + imagePath);
            }
        }

        graphicsImage = img;
    }
}
