package cz.linhy.clovece.game;

import cz.linhy.clovece.Functions;
import cz.linhy.clovece.customComponents.GamePane;
import cz.linhy.clovece.network.Socket;
import cz.linhy.clovece.windows.ColorSelectWindow;
import cz.linhy.clovece.windows.LobbyPlayersWindow;
import cz.linhy.clovece.windows.LobbyWindow;
import cz.linhy.clovece.windows.ReconnectWindow;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import javax.swing.event.ChangeEvent;
import java.util.Arrays;

/**
 * Třída hry. Zajišťuje hlavní herní logiku.
 */
public class Game {
    private static Game instance;
    private final GamePane gp;
    private Button shuffleButton;
    private Label lblAction;
    private Socket sock;
    private Stage m;

    /**
     * Posluchač socketu, předává zprávy ke zpracování
     */
    private javax.swing.event.ChangeListener socketListener = this::processEvents;

    /**
     * Konstruktor hry
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     */
    private Game(Stage m) {
        this.m = m;
        gp = GamePane.getInstance();
        gp.drawBoard();

    }

    /**
     * Vrátí instanci hry
     * @return instance hry
     */
    public static Game getInstance() {
        return instance;
    }

    /**
     * Vytvoří instanci hry
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     * @return instance hry
     */
    public static Game createInstance(Stage m) {
        if (instance == null) {
            instance = new Game(m);
        }

        return instance;
    }

    /**
     * Začne naslouchat na zadaném socketu
     * @param sock socket, na kterému instance naslouchá
     */
    public void startListening(Socket sock) {
        this.sock = sock;
        sock.addEventListener(socketListener);
    }

    /**
     * Vrátí herní panel
     * @return herní panel
     */
    public GamePane getGamePane() {
        return gp;
    }

    /**
     * Aktivuje tlačítko "Hodit kostkou"
     */
    public void enableButton() {
        shuffleButton.setDisable(false);
    }

    /**
     * Deaktivuje tlačítko "Hodit kostkou"
     */
    public void disableButton() {
        shuffleButton.setDisable(true);
    }

    /**
     * Vytvoří a vrátí box obsahující tlačítko a stavové zprávy
     * @return box obsahující tlačítko a stavové zprávy
     */
    public VBox createButtonBox() {
        VBox buttonBox = new VBox();
        buttonBox.setAlignment(Pos.CENTER);

        shuffleButton = new Button("Hodit kostkou!");
        shuffleButton.setFont(new Font(25));
        shuffleButton.setDisable(true);

        shuffleButton.setOnMouseClicked(e -> {
            shuffleButton.setDisable(true);
            gp.startShuffling();
            Socket.getSocket().throwCube();
        });

        lblAction = new Label("Čekáme na spuštění..");
        lblAction.setFont(new Font(15));

        buttonBox.getChildren().addAll(shuffleButton, lblAction);


        return buttonBox;
    }

    /**
     * Nastaví text stavové zprávy
     * @param text text stavové zprávy
     */
    private void setLabelText(String text) {
        lblAction.setText(text);
    }


    /**
     * Zpracuje příchozí zprávy.
     * @param e ChangeEvent obsahující zprávu rozdělenou na části
     */
    private void processEvents(ChangeEvent e) {
        processEvents((String[]) e.getSource());
    }

    /**
     * Zpracuje příchozí zprávy.
     * @param messageSplit zpráva rozdělená na části
     */
    private void processEvents(String[] messageSplit) {

        switch (messageSplit[0]) {
            case "NR":
                Socket.getSocket().setLobbyID(messageSplit[2]);
                Platform.runLater(() -> {
                    LobbyPlayersWindow.createWindow(m, sock.getPlayerName() + "-0", messageSplit[2], true);
                    LobbyWindow.close();
                });
                Platform.runLater(() -> ColorSelectWindow.createWindow(m, Integer.parseInt(messageSplit[1])));
                break;
            case "PU":
                if (LobbyPlayersWindow.getWindow() != null) {
                    Platform.runLater(() -> LobbyPlayersWindow.getWindow().changePlayers(messageSplit[2]));
                }

                if (ColorSelectWindow.isOpen()) {
                    Platform.runLater(() -> ColorSelectWindow.createWindow(m, Integer.parseInt(messageSplit[1])));
                }
                break;
            case "JR":
                if (messageSplit[1].equals("1")) {
                    Socket.getSocket().setLobbyID(messageSplit[3]);
                    Platform.runLater(() -> {
                        LobbyPlayersWindow.createWindow(Game.instance.m, messageSplit[4], messageSplit[3], false);
                        LobbyWindow.close();
                    });
                    Platform.runLater(() -> ColorSelectWindow.createWindow(Game.instance.m, Integer.parseInt(messageSplit[2])));
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Chyba");
                    alert.setHeaderText("Nepodařilo se připojit ke hře.");
                    alert.setContentText("Nepodařilo se připojit ke hře. Zkontrolujte kód hry.");
                    alert.initOwner(LobbyPlayersWindow.getWindow().getStage());

                    alert.showAndWait();
                }
                break;
            case "SR":
                if (messageSplit.length > 1) {
                    System.out.println("DEBUG: Showing alert");
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Chyba");
                        alert.setHeaderText("Hru nelze spustit");
                        alert.setContentText("Některý z hráčů nemá vybranou barvu!");
                        alert.initOwner(LobbyPlayersWindow.getWindow().getStage());

                        alert.showAndWait();
                    });

                } else {
                    Platform.runLater(() -> {
                        LobbyPlayersWindow.getWindow().getStage().close();
                    });
                    Platform.runLater(() -> {
                        GamePane.getInstance().startGame();
                    });
                }
                break;
            case "CA":
                Platform.runLater(() -> {
                    Color c = Functions.getColor(Integer.parseInt(messageSplit[1]));
                    Player plr = GamePane.getInstance().findPlayerByColor(c);
                    String text = "Právě je na tahu hráč " + plr.getName() + " (" + Functions.toColorName(c) + ")";
                    Game.getInstance().setLabelText(text);
                    if (GamePane.getInstance().getThisPlayer().equals(c)) {
                        Game.getInstance().enableButton();
                    }
                });
                break;
            case "CR":
                Platform.runLater(() -> {
                    Game.getInstance().getGamePane().setCube(Functions.getColor(Integer.parseInt(messageSplit[1])), Integer.parseInt(messageSplit[2]), Integer.parseInt(messageSplit[3]));
                });
                break;
            case "RR":
                Platform.runLater(() -> {
                    ReconnectWindow.connected();

                    if (messageSplit.length == 2) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Chyba");
                        alert.setHeaderText("Nepodařilo se obnovit připojení ke hře.");
                        alert.setContentText("Nepodařilo se obnovit připojení ke hře. Vytvořte novou hru.");
                        alert.initOwner(LobbyPlayersWindow.getWindow().getStage());

                        alert.showAndWait();
                        Socket.getSocket().quit();
                        return;
                    }

                    if (messageSplit.length >= 10) {
                        Color c = Functions.getColor(Integer.parseInt(messageSplit[1]));

                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[2]), Integer.parseInt(messageSplit[3]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[4]), Integer.parseInt(messageSplit[5]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[6]), Integer.parseInt(messageSplit[7]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[8]), Integer.parseInt(messageSplit[9]), null, 0, 0);
                    }

                    if (messageSplit.length >= 19) {
                        Color c = Functions.getColor(Integer.parseInt(messageSplit[10]));

                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[11]), Integer.parseInt(messageSplit[12]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[13]), Integer.parseInt(messageSplit[14]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[15]), Integer.parseInt(messageSplit[16]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[17]), Integer.parseInt(messageSplit[18]), null, 0, 0);
                    }

                    if (messageSplit.length >= 28) {
                        Color c = Functions.getColor(Integer.parseInt(messageSplit[19]));

                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[20]), Integer.parseInt(messageSplit[21]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[22]), Integer.parseInt(messageSplit[23]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[24]), Integer.parseInt(messageSplit[25]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[26]), Integer.parseInt(messageSplit[27]), null, 0, 0);
                    }

                    if (messageSplit.length >= 37) {
                        Color c = Functions.getColor(Integer.parseInt(messageSplit[28]));

                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[29]), Integer.parseInt(messageSplit[30]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[31]), Integer.parseInt(messageSplit[32]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[33]), Integer.parseInt(messageSplit[34]), null, 0, 0);
                        GamePane.getInstance().move(c, Integer.parseInt(messageSplit[35]), Integer.parseInt(messageSplit[36]), null, 0, 0);
                    }

                    GamePane.getInstance().reconnect();
                });

                break;
            case "DC":
                Platform.runLater(() -> {
                    Game.getInstance().getGamePane().disconnect();
                    ReconnectWindow.createWindow(m);
                });
                break;
            case "DCD":
                Platform.runLater(() -> {
                    Game.getInstance().getGamePane().disconnect();
                    LobbyPlayersWindow.close();
                    LobbyWindow.getInstance().show();
                });
                break;
            case "MR":
                Color c = Functions.getColor(Integer.parseInt(messageSplit[1]));
                int figure_index = Integer.parseInt(messageSplit[2]);
                int new_position = Integer.parseInt(messageSplit[3]);

                if (messageSplit.length == 4) {
                    Platform.runLater(() -> {
                        Game.getInstance().getGamePane().move(c, figure_index, new_position, null, Integer.MIN_VALUE, Integer.MIN_VALUE);
                    });
                } else {
                    Color kicked = Functions.getColor(Integer.parseInt(messageSplit[4]));
                    int kickedIndex = Integer.parseInt(messageSplit[5]);
                    int kickedNewPosition = Integer.parseInt(messageSplit[6]);
                    Platform.runLater(() -> {
                        Game.getInstance().getGamePane().move(c, figure_index, new_position, kicked, kickedIndex, kickedNewPosition);
                    });
                }
                break;

            case "E":
                Platform.runLater(() -> {
                    Color col = Functions.getColor(Integer.parseInt(messageSplit[2]));
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Konec hry!");
                    alert.setHeaderText("Konec hry!");
                    alert.setContentText("Vítězem se stává: " + messageSplit[1] + " (Barva: " + Functions.toColorName(col) + ")");
                    alert.initOwner(LobbyPlayersWindow.getWindow().getStage());

                    alert.showAndWait();

                    Game.getInstance().getGamePane().disconnect();
                    Game.getInstance().getGamePane().flush();
                    LobbyPlayersWindow.close();
                    LobbyWindow.getInstance().show();
                    Socket.getSocket().quit();
                });
                break;

            case "PL":
                Platform.runLater(() -> {
                    Color col = Functions.getColor(Integer.parseInt(messageSplit[1]));
                    Player p = GamePane.getInstance().findPlayerByColor(col);

                    p.setDisconnected(true);
                    GamePane.getInstance().drawBoard();
                });
                break;
            case "PRC":
                Color coll = Functions.getColor(Integer.parseInt(messageSplit[1]));
                Player pl = GamePane.getInstance().findPlayerByColor(coll);

                pl.setDisconnected(false);
                GamePane.getInstance().drawBoard();
                break;
            case "PONG":
                if (messageSplit.length > 1) {
                    processEvents(Arrays.copyOfRange(messageSplit, 1, messageSplit.length));
                }
                break;
            case "ERR":
                Socket.getSocket().quit();

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Chyba");
                alert.setHeaderText("Spojení se serverem bylo ukončeno z důvodu chyby přenosu.");
                alert.setContentText("Opakujte připojení.");
                alert.initOwner(LobbyPlayersWindow.getWindow().getStage());

                alert.showAndWait();
        }
    }
}





