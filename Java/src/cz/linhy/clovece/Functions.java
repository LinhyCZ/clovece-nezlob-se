package cz.linhy.clovece;

import javafx.scene.paint.Color;

/**
 * Třída s pomocnými statickými funkcemi
 */
public class Functions {
    /**
     * Vrátí instanci barvy podle předaného čísla
     * @param colorNumber číslo barvy
     * @return instance barvy
     */
    public static Color getColor(int colorNumber) {
        if (colorNumber == 16) {
            return Color.LIME;
        } else if (colorNumber == 8) {
            return Color.BLUE;
        } else if (colorNumber == 4) {
            return Color.YELLOW;
        } else if (colorNumber == 2) {
            return Color.RED;
        } else {
            return Color.BLACK;
        }
    }

    /**
     * Převede instanci barvy na české pojmenování
     * @param c instance barvy
     * @return String obsahující název barvy
     */
    public static String toColorName(Color c) {
        if (c.equals(Color.LIME)) {
            return "Zelená";
        } else if (c.equals(Color.RED)) {
            return "Červená";
        } else if (c.equals(Color.BLUE)) {
            return "Modrá";
        } else {
            return "Žlutá";
        }
    }
}
