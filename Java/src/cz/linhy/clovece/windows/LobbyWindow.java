package cz.linhy.clovece.windows;

import cz.linhy.clovece.game.Game;
import cz.linhy.clovece.network.Socket;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Hlavní okno lobby. Zobrazuje možnosti pro zadání IP adresy a portu, možnost pro zadání kódu a připojení a vytvoření hry
 */
public class LobbyWindow {
    private final Stage m;
    private Stage st;
    private Socket sock;
    private ChangeListener<Number> listener;

    private static LobbyWindow instance;

    /**
     * Zavře okno
     */
    public static void close() {
        if (instance != null) {
            instance.st.close();
        }
    }

    /**
     * Zobrazí okno
     */
    public void show() {
        st.show();
    }

    /**
     * Konstruktor, vytvoří okno
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     */
    private LobbyWindow(Stage m) {
        this.m = m;
        createStage();
    }

    /**
     * Vrátí instanci okna
     * @return instance okna
     */
    public static LobbyWindow getInstance() {
        return instance;
    }

    /**
     * Vytvoří instanci okna
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     * @return instance okna
     */
    public static LobbyWindow createInstance(Stage m) {
        LobbyWindow.instance = new LobbyWindow(m);

        return instance;
    };

    /**
     * Vykreslí okno hlavního lobby
     */
    private void createStage() {
        BorderPane rootPane = new BorderPane();
        Scene sc = new Scene(rootPane);

        st = new Stage();
        st.setResizable(false);
        st.setScene(sc);
        st.setTitle("Člověče, nezlob se!");

        st.setOnCloseRequest((e) -> {
            Platform.exit();
        });

        HBox lblBox = new HBox();
        lblBox.setAlignment(Pos.CENTER);

        Label lbl = new Label("Člověče, nezlob se!");
        lbl.setFont(new Font(30));
        lblBox.getChildren().addAll(lbl);

        rootPane.setTop(lblBox);

        VBox IPAddressBox = new VBox();
        IPAddressBox.setAlignment(Pos.CENTER);
        Label IPAddressLabel = new Label("IP Adresa a port:");
        IPAddressLabel.setFont(new Font(15));
        TextField IPText = new TextField("127.0.0.1:9652");
        IPText.setMaxWidth(200);
        IPText.setAlignment(Pos.CENTER);
        IPAddressBox.getChildren().addAll(IPAddressLabel, IPText);
        rootPane.setTop(IPAddressBox);

        VBox nameInputBox = new VBox();
        nameInputBox.setAlignment(Pos.CENTER);
        Label nameInputLabel = new Label("Jméno hráče:");
        nameInputLabel.setFont(new Font(15));
        TextField nameText = new TextField("Player");
        nameText.setMaxWidth(200);
        nameText.setAlignment(Pos.CENTER);
        nameInputBox.getChildren().addAll(nameInputLabel, nameText);
        rootPane.setCenter(nameInputBox);

        HBox lobbyBox = new HBox();
        VBox createGameBox = new VBox();
        createGameBox.setPadding(new Insets(20, 0, 0, 0));
        createGameBox.setPrefWidth(200);
        createGameBox.setAlignment(Pos.CENTER);

        Label createNewGameLabel = new Label("Vytvořit novou hru.");
        createNewGameLabel.setFont(new Font(15));

        Label createNewGameSpacer = new Label("");
        createNewGameSpacer.setPrefHeight(65);

        Button createNewGameBtn = new Button();
        createNewGameBtn.setText("Založit místnost!");
        createNewGameBtn.setPrefWidth(150);
        createNewGameBtn.setOnMouseClicked((e) -> {createNewGame(IPText.getText(), nameText.getText());});
        createGameBox.getChildren().addAll(createNewGameLabel, createNewGameSpacer, createNewGameBtn);

        VBox joinGameBox = new VBox();
        joinGameBox.setPadding(new Insets(20, 0, 0, 0));
        joinGameBox.setPrefWidth(200);
        joinGameBox.setAlignment(Pos.CENTER);

        Label gameCodeLabel = new Label("Zadejte kód hry.");
        gameCodeLabel.setFont(new Font(15));
        TextField gameCodeInput = new TextField();
        gameCodeInput.setAlignment(Pos.CENTER);
        gameCodeInput.setFont(new Font(30));
        gameCodeInput.setMaxWidth(150);
        gameCodeInput.setPrefHeight(65);
        gameCodeInput.textProperty().addListener((obs, old, newText) -> {
            String text = gameCodeInput.getText().toUpperCase();
            text = text.replaceAll("[^a-zA-Z0-9]", "");
            if (text.length() > 5) {
                text = text.substring(0, 5);
            }
            gameCodeInput.setText(text);
        });
        Button joinGameBtn = new Button();
        joinGameBtn.setMaxWidth(150);
        joinGameBtn.setText("Připojit se!");
        joinGameBtn.setOnMouseClicked((e) -> {joinGame(IPText.getText(), gameCodeInput.getText(), nameText.getText());});
        joinGameBox.getChildren().addAll(gameCodeLabel, gameCodeInput, joinGameBtn);
        lobbyBox.getChildren().addAll(createGameBox, joinGameBox);
        lobbyBox.setPadding(new Insets(0, 0, 20, 0));
        rootPane.setBottom(lobbyBox);

        st.setX(m.getX() + m.getWidth() / 2 - st.getWidth() /2);
        st.setY( m.getY() + m.getHeight() / 2 - st.getHeight() /2);

        st.setAlwaysOnTop(true);

        m.xProperty().addListener(changeListner());
        m.yProperty().addListener(changeListner());
        m.widthProperty().addListener(changeListner());
        m.heightProperty().addListener(changeListner());
        st.xProperty().addListener(changeListner());
        st.yProperty().addListener(changeListner());
    }

    /**
     * Vytvoří Socket na zadané IP adrese a připojí se k serveru s daným jménem
     * @param IP IP adresa a port serveru
     * @param name Jméno hráče
     */
    private void createSocketAndLogin(String IP, String name) {
        ConnectWindow.createWindow(m);

        sock = Socket.createSocket(IP);
        Game.getInstance().startListening(sock);
        sock.login(name);
    }

    /**
     * Vytvoří Socket na zadané IP adrese a připojí se k serveru s daným jménem
     * @param IP IP adresa a port serveru
     * @param name Jméno hráče
     * @param code Kód hry, ke které se hráč připojuje
     */
    private void joinGame(String IP, String code, String name) {
        createSocketAndLogin(IP, name);
        sock.joinGame(code);
    }

    /**
     * Vytvoří Socket na zadané IP adrese a připojí se k serveru s daným jménem a vytvoří hru
     * @param IP IP adresa a port serveru
     * @param name Jméno hráče
     */
    private void createNewGame(String IP, String name) {
        createSocketAndLogin(IP, name);
        sock.createNewGame();
    }


    /**
     * Vytvoří posluchače, který sleduje polohu mateřského okna (předané v konstruktoru)
     * a upravuje polohu tohoto okna vzhledem k němu.
     * @return Instance posluchače
     */
    private ChangeListener<? super Number> changeListner() {
        if (listener == null) {
            listener = (observable, oldValue, newValue) -> {
                st.setX(m.getX() + m.getWidth() / 2 - st.getWidth() /2);
                st.setY( m.getY() + m.getHeight() / 2 - st.getHeight() /2);
            };
        }

        return listener;
    }
}
