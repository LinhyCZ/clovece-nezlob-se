package cz.linhy.clovece.windows;

import cz.linhy.clovece.game.Player;
import cz.linhy.clovece.network.Socket;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Třída okna, které se zobrazí při navazování spojení.
 */
public class ConnectWindow {
    private BorderPane rootPane;
    private static ConnectWindow instance;
    private Stage st;
    private Stage m;
    private ChangeListener<Number> listener;

    /**
     * Konstruktor, vytvoří a zobrazí okno pro opětovné připojení.
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     */
    private ConnectWindow(Stage m) {
        this.m = m;
        createStage();
    }

    /**
     * Spustí časovač na zavření okna
     */
    public static void close() {
        Platform.runLater(() -> {
            Label lbl = new Label("Připojení selhalo! Ověřte IP adresu.");
            lbl.setPadding(new Insets(20,100,20,100));
            lbl.setFont(new Font(18));
            instance.rootPane.setCenter(lbl);

            instance.st.sizeToScene();
            instance.st.setX(instance.m.getX() + instance.m.getWidth() / 2 - instance.st.getWidth() /2);
            instance.st.setY( instance.m.getY() + instance.m.getHeight() / 2 - instance.st.getHeight() /2);

            Timer timer = new Timer("CloseWindow");
            timer.schedule(instance.createNewTimerTask(), 5000);
        });
    }

    /**
     * Vytvoří a zobrazí okno pro opětovné připojení.
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     */
    public static void createWindow(Stage m) {
        if (instance != null) {
            instance.st.close();
        }

        instance = new ConnectWindow(m);
    }

    /**
     * Vytvoří Stage pro okno a vykreslí všechny prvky. Spustí opětovné připojení.
     */
    private void createStage() {
        rootPane = new BorderPane();
        //Label lbl = new Label("Připojení selhalo! Ověřte IP adresu.");
        Label lbl = new Label("Připojování k serveru.");
        lbl.setPadding(new Insets(20,100,20,100));
        lbl.setFont(new Font(18));
        rootPane.setCenter(lbl);

        Scene sc = new Scene(rootPane);

        st = new Stage();
        //st.setResizable(false);
        st.setScene(sc);
        st.setTitle("Připojení k serveru!");
        st.setX(m.getX() + m.getWidth() / 2 - st.getWidth() /2);
        st.setY( m.getY() + m.getHeight() / 2 - st.getHeight() /2);

        st.setAlwaysOnTop(true);

        m.xProperty().addListener(changeListner());
        m.yProperty().addListener(changeListner());
        m.widthProperty().addListener(changeListner());
        m.heightProperty().addListener(changeListner());
        st.xProperty().addListener(changeListner());
        st.yProperty().addListener(changeListner());
        st.show();
    }

    /**
     * Vytvoří úlohu opětovného připojení, která se předá Timeru
     * @return instance úlohy opětovného připojení
     */
    private TimerTask createNewTimerTask() {
        return new TimerTask() {
            public void run() {
                Platform.runLater(() -> {
                    instance.st.close();
                });
            }
        };
    };

    /**
     * Provede akci při správném připojení
     */
    public static void connected() {
        if (instance != null) {
            Platform.runLater(() -> {
                instance.st.close();
            });
        }
    }

    /**
     * Vytvoří posluchače, který sleduje polohu mateřského okna (předané v konstruktoru)
     * a upravuje polohu tohoto okna vzhledem k němu.
     * @return Instance posluchače
     */
    private ChangeListener<? super Number> changeListner() {
        if (listener == null) {
            listener = (observable, oldValue, newValue) -> {
                st.setX(m.getX() + m.getWidth() / 2 - st.getWidth() /2);
                st.setY( m.getY() + m.getHeight() / 2 - st.getHeight() /2);
            };
        }

        return listener;
    }

}
