package cz.linhy.clovece.windows;

import cz.linhy.clovece.customComponents.GamePane;
import cz.linhy.clovece.network.Socket;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Okno pro výběr barev
 */
public class ColorSelectWindow {
    private static ColorSelectWindow instance;
    private Stage st;
    private Stage m;
    private ChangeListener<Number> listener;
    private static boolean isOpen = true;
    private final int colorsAvailableFinal;

    /**
     * Konstruktor, zajistí vytvoření okna
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     * @param colorsAvailable číslo dostupných barev
     */
    private ColorSelectWindow(Stage m, int colorsAvailable) {
        this.m = m;
        this.colorsAvailableFinal = colorsAvailable;
        createStage();
    }

    /**
     * Vrátí zda je okno otevřeno
     * @return true = okno otevřeno, false = okno zavřeno
     */
    public static boolean isOpen() {
        return isOpen;
    }

    /**
     * Zavře okno
     */
    public static void close() {
        instance.st.close();
        isOpen = false;
    }

    /**
     * Vytvoří instanci okna pro výběr barvy
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     * @param colorsAvailable číslo dostupných barev
     */
    public static void createWindow(Stage m, int colorsAvailable) {
        if (instance != null) {
            instance.st.close();
        }

        isOpen = true;

        instance = new ColorSelectWindow(m, colorsAvailable);
    }

    /**
     * Vykreslí okno pro výběr barvy
     */
    private void createStage() {
        BorderPane rootPane = new BorderPane();
        Label lbl = new Label("Vyberte si svoji barvu!");
        lbl.setPadding(new Insets(20,100,20,100));
        lbl.setFont(new Font(18));
        rootPane.setCenter(lbl);

        int colorsAvailable = colorsAvailableFinal;

        HBox colorButtons = new HBox();

        if (colorsAvailable >= 16) {
            colorButtons.getChildren().add(createButton(Color.LIME));
            colorsAvailable -= 16;
        }

        if (colorsAvailable >= 8) {
            colorButtons.getChildren().add(createButton(Color.BLUE));
            colorsAvailable -= 8;
        }

        if (colorsAvailable >= 4) {
            colorButtons.getChildren().add(createButton(Color.YELLOW));
            colorsAvailable -= 4;
        }

        if (colorsAvailable >= 2) {
            colorButtons.getChildren().add(createButton(Color.RED));
        }

        colorButtons.setAlignment(Pos.CENTER);
        colorButtons.setSpacing(10);
        colorButtons.setPadding(new Insets(0, 0, 20, 0));

        rootPane.setBottom(colorButtons);

        Scene sc = new Scene(rootPane);

        st = new Stage();
        st.setResizable(false);
        st.setScene(sc);
        st.setTitle("Vyberte si svoji barvu!");
        st.setX(m.getX() + m.getWidth() / 2 - st.getWidth() /2);
        st.setY( m.getY() + m.getHeight() / 2 - st.getHeight() /2);

        st.setAlwaysOnTop(true);

        st.setOnCloseRequest(e -> {
            ColorSelectWindow.createWindow(m, colorsAvailableFinal);
        });

        m.xProperty().addListener(changeListner());
        m.yProperty().addListener(changeListner());
        m.widthProperty().addListener(changeListner());
        m.heightProperty().addListener(changeListner());
        st.xProperty().addListener(changeListner());
        st.yProperty().addListener(changeListner());
        st.show();
    }

    /**
     * Vytvoří tlačítko pro výběr dané barvy
     * @param c Barva tlačítka
     * @return Instance tlačítka
     */
    private Button createButton(Color c) {
        Button btn = new Button();
        btn.setBackground(new Background(new BackgroundFill(c, CornerRadii.EMPTY, Insets.EMPTY)));

        btn.setPrefSize(50, 50);
        btn.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), BorderWidths.DEFAULT)));

        btn.setOnMouseClicked(e -> {
            isOpen = false;
            st.close();
            GamePane.getInstance().setThisPlayer(c);
            if (c.equals(Color.LIME)) Socket.getSocket().sendColor(16);
            if (c.equals(Color.BLUE)) Socket.getSocket().sendColor(8);
            if (c.equals(Color.YELLOW)) Socket.getSocket().sendColor(4);
            if (c.equals(Color.RED)) Socket.getSocket().sendColor(2);
        });

        return btn;
    }

    /**
     * Vytvoří posluchače, který sleduje polohu mateřského okna (předané v konstruktoru)
     * a upravuje polohu tohoto okna vzhledem k němu.
     * @return Instance posluchače
     */
    private ChangeListener<? super Number> changeListner() {
        if (listener == null) {
            listener = (observable, oldValue, newValue) -> {
                st.setX(m.getX() + m.getWidth() / 2 - st.getWidth() /2);
                st.setY( m.getY() + m.getHeight() / 2 - st.getHeight() /2);
            };
        }

        return listener;
    }

}
