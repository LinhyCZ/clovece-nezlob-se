package cz.linhy.clovece.windows;

import cz.linhy.clovece.network.Socket;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Třída okna, které se zobrazí po ztrátě spojení. Zajišťuje logiku s obnovením spojení.
 */
public class ReconnectWindow {
    private BorderPane rootPane;
    private static ReconnectWindow instance;
    private Stage st;
    private Stage m;
    private int numberOfTries = 1;
    private Label numberOfTriesLabel;
    private HBox buttonBox;
    private int loginFailCounter = 0;
    private ChangeListener<Number> listener;
    private Timer timer = new Timer("ReconnectTimer");
    private TimerTask task;

    /**
     * Konstruktor, vytvoří a zobrazí okno pro opětovné připojení.
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     */
    private ReconnectWindow(Stage m) {
        this.m = m;
        createStage();
    }

    /**
     * Zavře okno
     */
    public static void close() {
        instance.st.close();
    }

    /**
     * Vytvoří a zobrazí okno pro opětovné připojení.
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     */
    public static void createWindow(Stage m) {
        if (instance != null) {
            instance.st.close();
        }

        instance = new ReconnectWindow(m);
    }

    /**
     * Vytvoří Stage pro okno a vykreslí všechny prvky. Spustí opětovné připojení.
     */
    private void createStage() {
        rootPane = new BorderPane();
        Label lbl = new Label("Připojení selhalo! Provádím opětovné připojení!");
        lbl.setPadding(new Insets(20,100,20,100));
        lbl.setFont(new Font(18));
        rootPane.setTop(lbl);

        numberOfTriesLabel = new Label("Poček pokusů o připojení: " + numberOfTries);
        numberOfTriesLabel.setPadding(new Insets(20,100,20,100));
        numberOfTriesLabel.setFont(new Font(18));
        rootPane.setCenter(numberOfTriesLabel);

        Scene sc = new Scene(rootPane);

        st = new Stage();
        //st.setResizable(false);
        st.setScene(sc);
        st.setTitle("Opětovné připojení k serveru!");
        st.setX(m.getX() + m.getWidth() / 2 - st.getWidth() /2);
        st.setY( m.getY() + m.getHeight() / 2 - st.getHeight() /2);

        st.setAlwaysOnTop(true);

        st.setOnCloseRequest(e -> {
            st.show();
        });

        buttonBox = new HBox();
        buttonBox.setAlignment(Pos.CENTER);
        Button button = new Button("Zkusit znovu!");
        button.setFont(new Font(25));
        buttonBox.getChildren().add(button);

        button.setOnMouseClicked((e) -> {
            numberOfTries++;
            timer = new Timer("ReconnectTimer");
            task = createNewTimerTask();
            timer.scheduleAtFixedRate(task, 0, 1000);
            numberOfTriesLabel.setText("Poček pokusů o připojení: " + numberOfTries);

            rootPane.setBottom(null);
            st.sizeToScene();
        });

        task = createNewTimerTask();
        timer.scheduleAtFixedRate(task, 0, 1000);

        m.xProperty().addListener(changeListner());
        m.yProperty().addListener(changeListner());
        m.widthProperty().addListener(changeListner());
        m.heightProperty().addListener(changeListner());
        st.xProperty().addListener(changeListner());
        st.yProperty().addListener(changeListner());
        st.show();
    }

    /**
     * Vytvoří úlohu opětovného připojení, která se předá Timeru
     * @return instance úlohy opětovného připojení
     */
    private TimerTask createNewTimerTask() {
        return new TimerTask() {
            public void run() {
                System.out.println("WARNING: Not reconnected, retrying!");

                loginFailCounter++;
                if (loginFailCounter == 5) {
                    Socket.getSocket().reconnect();
                    loginFailCounter = 0;
                    numberOfTries++;

                    Platform.runLater(() -> numberOfTriesLabel.setText("Poček pokusů o připojení: " + numberOfTries));
                }

                if (numberOfTries != 0 && numberOfTries % 5 == 0) {
                    System.out.println("ERROR: Server not available!");
                    Platform.runLater(() -> {
                        loginFailCounter = 0;
                        numberOfTriesLabel.setText("Nepodařilo se připojit k serveru!");
                        rootPane.setBottom(buttonBox);
                        st.sizeToScene();
                    });

                    timer.cancel();
                }
            }
        };
    }

    /**
     * Provede akci při správném opětovném připojení
     */
    public static void connected() {
        if (instance != null) {
            instance.timer.cancel();
            close();
        }
    }

    /**
     * Vytvoří posluchače, který sleduje polohu mateřského okna (předané v konstruktoru)
     * a upravuje polohu tohoto okna vzhledem k němu.
     * @return Instance posluchače
     */
    private ChangeListener<? super Number> changeListner() {
        if (listener == null) {
            listener = (observable, oldValue, newValue) -> {
                st.setX(m.getX() + m.getWidth() / 2 - st.getWidth() /2);
                st.setY( m.getY() + m.getHeight() / 2 - st.getHeight() /2);
            };
        }

        return listener;
    }

}
