package cz.linhy.clovece.windows;

import cz.linhy.clovece.Functions;
import cz.linhy.clovece.customComponents.GamePane;
import cz.linhy.clovece.game.Player;
import cz.linhy.clovece.network.Socket;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * Okno, ve kterém je vykresleno Lobby, ke kterému je hráč aktuálně připojený
 */
public class LobbyPlayersWindow {
    private static LobbyPlayersWindow instance;
    private final Stage m;
    private Stage st;
    private BorderPane rootPane;
    private ChangeListener<Number> listener;

    /**
     * Konstruktor, vytvoří okno
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     * @param connectedPlayers řetězec obsahující jména a barvy hráčů
     * @param lobbyID kód Lobby
     * @param leader true = hráč je leader, false = hráč není leader
     */
    private LobbyPlayersWindow(Stage m, String connectedPlayers, String lobbyID, boolean leader) {
        this.m = m;
        showWindow(connectedPlayers, lobbyID, leader);
    }

    /**
     * Vrátí instanci okna
     * @return instance okna
     */
    public static LobbyPlayersWindow getWindow() {
        return instance;
    }

    /**
     * Vytvoří instanci okna lobby
     * @param m Stage, ke které se zarovnává pozice tohoto okna
     * @param connectedPlayers řetězec obsahující jména a barvy hráčů
     * @param lobbyID kód Lobby
     * @param leader true = hráč je leader, false = hráč není leader
     */
    public static void createWindow(Stage m, String connectedPlayers, String lobbyID, boolean leader) {
        instance = new LobbyPlayersWindow(m, connectedPlayers, lobbyID, leader);
    }

    /**
     * Vykreslí okno s hráči v daném lobby. Pokud je hráč leader, zobrazí se mu tlačítko pro spuštění hry
     * @param connectedPlayers řetězec obsahující jména a barvy hráčů
     * @param lobbyID kód Lobby
     * @param leader true = hráč je leader, false = hráč není leader
     */
    private void showWindow(String connectedPlayers, String lobbyID, boolean leader) {
        rootPane = new BorderPane();
        Scene sc = new Scene(rootPane);

        rootPane.setMinHeight(400);
        rootPane.setMinWidth(400);
        rootPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

        VBox lobbyIDBox = new VBox();
        Label lobbyIDLabel = new Label("Lobby: " + lobbyID);
        lobbyIDLabel.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, FontPosture.REGULAR,  20));

        lobbyIDBox.setAlignment(Pos.CENTER);
        lobbyIDBox.setPadding(new Insets(10,0,10,0));
        lobbyIDBox.getChildren().add(lobbyIDLabel);
        rootPane.setTop(lobbyIDBox);

        rootPane.setCenter(generatePlayersHBox(connectedPlayers));

        VBox startGameBox = new VBox();
        if (leader) {
            Button startGame = new Button("Spustit hru");
            startGame.setFont(new Font(20));
            startGame.setPadding(new Insets(10, 20, 10, 20));
            startGame.setOnMouseClicked((e) -> {
                Socket.getSocket().startGame();
            });

            startGameBox.getChildren().add(startGame);
        } else {
            Label startGameLabel = new Label("Čeká se na spuštění hry..");
            startGameLabel.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, FontPosture.REGULAR,  20));
            startGameBox.getChildren().add(startGameLabel);
        }

        startGameBox.setAlignment(Pos.CENTER);
        startGameBox.setPadding(new Insets(10,0,10,0));
        rootPane.setBottom(startGameBox);

        st = new Stage();
        st.setResizable(false);
        st.setScene(sc);
        st.setTitle("Lobby: " + lobbyID);
        st.setX(m.getX() + m.getWidth() / 2 - st.getWidth() /2);
        st.setY( m.getY() + m.getHeight() / 2 - st.getHeight() /2);

        st.setAlwaysOnTop(true);

        st.setOnCloseRequest((e) -> {
            Socket.getSocket().quit();
            if (ColorSelectWindow.isOpen()) {
                ColorSelectWindow.close();
            }
            LobbyWindow.getInstance().show();
        });

        m.xProperty().addListener(changeListner());
        m.yProperty().addListener(changeListner());
        m.widthProperty().addListener(changeListner());
        m.heightProperty().addListener(changeListner());
        st.xProperty().addListener(changeListner());
        st.yProperty().addListener(changeListner());

        st.show();
    }

    /**
     * Aktualizuje hráče v lobby
     * @param connectedPlayers řetězec obsahující jména a barvy hráčů
     */
    public void changePlayers(String connectedPlayers) {
        rootPane.setCenter(generatePlayersHBox(connectedPlayers));
    }

    /**
     * Vytvoří Box, který obsahuje jména připojených hráčů
     * @param connectedPlayers řetězec obsahující jména a barvy hráčů
     * @return instance Boxu
     */
    private VBox generatePlayersHBox(String connectedPlayers) {
        List<Player> playersList = new ArrayList<>();

        String[] players = connectedPlayers.split(",");
        VBox playersBox = new VBox();
        playersBox.setAlignment(Pos.CENTER);

        Label playerLbl = new Label("Hráči");
        playerLbl.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, FontPosture.REGULAR, 30));
        playersBox.getChildren().add(playerLbl);
        for (String player : players) {

            String[] split = player.split("-");

            String name = split[0];
            int color = Integer.parseInt(split[1]);
            Color c = Functions.getColor(color);

            playersList.add(new Player(name, c));

            Label playerLabel = new Label(name);
            playerLabel.setFont(new Font(30));
            playerLabel.setTextFill(c);
            playersBox.getChildren().add(playerLabel);
        }

        GamePane.getInstance().changePlayers(playersList);
        return playersBox;
    }

    /**
     * Vytvoří posluchače, který sleduje polohu mateřského okna (předané v konstruktoru)
     * a upravuje polohu tohoto okna vzhledem k němu.
     * @return Instance posluchače
     */
    private ChangeListener<? super Number> changeListner() {
        if (listener == null) {
            listener = (observable, oldValue, newValue) -> {
                st.setX(m.getX() + m.getWidth() / 2 - st.getWidth() /2);
                st.setY( m.getY() + m.getHeight() / 2 - st.getHeight() /2);
            };
        }

        return listener;
    }

    /**
     * Vrátí stage okna
     * @return stage okna
     */
    public Stage getStage() {
        return st;
    }

    /**
     * Zavře okno
     */
    public static void close() {
        LobbyPlayersWindow.getWindow().getStage().close();
    }
}
