package cz.linhy.clovece.customComponents;

import cz.linhy.clovece.game.Figure;
import cz.linhy.clovece.game.Game;
import cz.linhy.clovece.game.Player;
import cz.linhy.clovece.game.drawer.GameDrawer;
import cz.linhy.clovece.game.gameElements.AllPoints;
import cz.linhy.clovece.game.gameElements.Graphics;
import cz.linhy.clovece.network.Socket;
import javafx.animation.AnimationTimer;
import javafx.geometry.Pos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Třída herního panelu, která zajišťuje vykreslení hry v okně a logiku s tím spojenou
 */
public class GamePane extends HBox {
    private static GamePane instance;
    private CanvasPane cp;
    private GraphicsContext gc;
    private GameDrawer gd;
    private boolean started = false;
    private boolean awaitingMove = false;

    private AnimationTimer timer;

    private List<Player> players = new ArrayList<>();
    private Color thisPlayer = null;

    /**
     * Konstruktor, vytvoří instanci herního panelu
     */
    private GamePane() {
        instance = this;
        cp = new CanvasPane();
        gc = cp.getGraphicsContext2D();
        gd = new GameDrawer(cp);

        this.getChildren().add(cp);
        this.setAlignment(Pos.CENTER);

        cp.getCanvasWidthProperty().addListener(e -> drawBoard());
        cp.getCanvasHeightProperty().addListener(e -> drawBoard());

        cp.setOnMouseClicked(e -> canvasClick(e));

        this.widthProperty().addListener(e -> sizeChange());
        this.heightProperty().addListener(e -> sizeChange());
    }

    /**
     * Vrátí instanci herního panelu
     * @return instance herního panelu
     */
    public static GamePane getInstance() {
        if (instance == null) {
            instance = new GamePane();
        }

        return instance;
    }

    /**
     * Vymění seznam hráčů
     * @param playersList nový seznam hráčů
     */
    public void changePlayers(List<Player> playersList) {
        this.players = playersList;
        drawBoard();
    }

    /**
     * Nastaví barvu tohoto hráče
     * @param thisPlayer barva tohoto hráče
     */
    public void setThisPlayer(Color thisPlayer) {
        this.thisPlayer = thisPlayer;
    }

    /**
     * Zjistí, zda kliknutí na herní pole se neprotíná s nějakou hráčovo figurkou.
     * Pokud ano a hráč je na řadě, odešle zprávu s indexem figurky, která se posouvá
     * @param e MouseEvent obsahující pozici kliknutí
     */
    private void canvasClick(MouseEvent e) {
        if (awaitingMove) {
            for (Player p : players) {
                if (p.getColor() == thisPlayer) {
                    for (Figure f : p.getFigures()) {
                        if (f.intersect(new Point(gd.fromScreenToGame(e.getX()), gd.fromScreenToGame(gd.switchY(e.getY()))))) {
                            awaitingMove = false;
                            Socket.getSocket().sendMoveReponse(f.getId());
                        }
                    }
                }
            }
        }
    }

    /**
     * Provede akce po začátku hry
     */
    public void startGame() {
        started = true;
        drawBoard();
    }

    /**
     * Vykreslí herní plochu
     */
    public void drawBoard() {
        erase();
        cp.requestFocus();

        /* Draw red square around home */
        gd.setFill(Color.BLACK);
        gd.fillRect(67.5, 67.5, 15, 60);
        gd.setFill(Color.RED);
        gd.fillRect(68.5, 66.5, 13, 58);

        /* Draw yellow square around home */
        gd.setFill(Color.BLACK);
        gd.fillRect(7.5, 82.5, 60, 15);
        gd.setFill(Color.YELLOW);
        gd.fillRect(8.5, 81.5, 58, 13);

        /* Draw blue square around home */
        gd.setFill(Color.BLACK);
        gd.fillRect(67.5, 142.5, 15, 60);
        gd.setFill(Color.BLUE);
        gd.fillRect(68.5,  141.5, 13, 58);

        /* Draw green square around home */
        gd.setFill(Color.BLACK);
        gd.fillRect(82.5, 82.5, 60, 15);
        gd.setFill(Color.LIME);
        gd.fillRect(83.5, 81.5, 58, 13);

        for (AllPoints point : AllPoints.values()) {
            point.getBp().draw(gd);
        }

        if (!started) {
            gd.drawImage(Graphics.values()[5].getGraphicsImage(), 27.5, 42.5, 15, 15); //RED
            gd.drawImage(Graphics.values()[5].getGraphicsImage(), 27.5, 122.5, 15, 15); //YELLOW
            gd.drawImage(Graphics.values()[5].getGraphicsImage(), 107.5, 122.5, 15, 15); //BLUE
            gd.drawImage(Graphics.values()[5].getGraphicsImage(), 107.5, 42.5, 15, 15); //GREEN
        }

        for (Player p : players) {
            if (!p.getColor().equals(Color.BLACK)) {
                for (Figure f : p.getFigures()) {
                    f.draw(gd);
                }
            }
        }
    }

    /**
     * Vymaže veškeré vykreslené objekty
     */
    private void erase() {
        gc.setFill(cp.getBackgroundColor());
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, cp.getCanvasSize(), cp.getCanvasSize());
    }


    /**
     * Akce po změně velikosti okna
     */
    private void sizeChange() {
        double width = this.getWidth();
        double height = this.getHeight();

        double squareSize = Math.min(width, height);

        cp.setPrefHeight(squareSize);
        cp.setPrefWidth(squareSize);

        drawBoard();
    }

    /**
     * Posune figurku na daný index. Pokud je při posunu vyhozena figurka, je posunuta do domečku
     * @param c Barva hráče, jehož figurka se posouvá
     * @param figure_index Index figurky, která se posouvá
     * @param new_position Nová pozice, na kterou figurka se posouvá
     * @param kickedColor Barva hráče, jehož figurka byla vyhozena
     * @param kickedIndex Index figurky, která byla vyhozena
     * @param kickedNewPosition Nová pozice (v domečku) vyhozené figurky
     */
    public void move(Color c, int figure_index, int new_position, Color kickedColor, int kickedIndex, int kickedNewPosition) {
        Player p = findPlayerByColor(c);
        p.getFigures()[figure_index].moveTo(AllPoints.values()[new_position]);

        if (kickedColor != null) {
            Player kicked = findPlayerByColor(kickedColor);
            kicked.getFigures()[kickedIndex].moveTo(AllPoints.values()[kickedNewPosition]);
        }

        drawBoard();
    }

    /**
     * Vrátí instanci hráče ve hře s danou barvou.
     * @param c Barva hráče
     * @return Instance hráče, null pokud neexistuje
     */
    public Player findPlayerByColor(Color c) {
        for (Player p : players) {
            if (p.getColor().equals(c)) {
                return p;
            }
        }
        return null;
    }

    /**
     * Spustí animaci otáčení kostky
     */
    public void startShuffling() {
        timer = new AnimationTimer() {
            @Override
            public void handle(long currentNanoTime) {
                instance.drawBoard();

                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    // Do nothing
                }
            }
        };

        timer.start();
    }

    /**
     * Zastaví animaci otáčení kostky
     */
    public void stopShuffling() {
        if (timer != null) {
            timer.stop();
        }
    }

    /**
     * Nastaví kostku na dané číslo. Kostka se zobrazí u hráče zadané barvy.
     * @param color Barva hráče
     * @param diceNumber Číslo kostky
     * @param canMove Určí, zda hráč má alespoň 1 validní tah
     */
    public void setCube(Color color, int diceNumber, int canMove) {
        stopShuffling();

        if (color.equals(thisPlayer) && canMove == 1) {
            awaitingMove = true;
        }

        drawBoard();

        if (color.equals(Color.RED)) {
            gd.drawImage(Graphics.values()[diceNumber - 1].getGraphicsImage(), 27.5, 42.5, 15, 15); //RED
        } else if (color.equals(Color.YELLOW)) {
            gd.drawImage(Graphics.values()[diceNumber - 1].getGraphicsImage(), 27.5, 122.5, 15, 15); //YELLOW
        } else if (color.equals(Color.BLUE)) {
            gd.drawImage(Graphics.values()[diceNumber - 1].getGraphicsImage(), 107.5, 122.5, 15, 15); //BLUE
        } else {
            gd.drawImage(Graphics.values()[diceNumber - 1].getGraphicsImage(), 107.5, 42.5, 15, 15); //GREEN
        }
    }

    /**
     * Zpracuje akce po odpojení od serveru
     */
    public void disconnect() {
        Game.getInstance().disableButton();
        awaitingMove = false;
        started = false;

        drawBoard();
    }

    /**
     * Zpracuje akce po opětovném připojení
     */
    public void reconnect() {
        started = true;
        drawBoard();
    }

    /**
     * Vrátí barvu tohoto hráče
     * @return barva tohoto hráče
     */
    public Color getThisPlayer() {
        return thisPlayer;
    }

    /**
     * Vyprázdní hráče pro vykreslování
     */
    public void flush() {
        players = new ArrayList<>();
        started = false;

        drawBoard();
    }
}
