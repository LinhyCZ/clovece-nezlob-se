package cz.linhy.clovece.customComponents;

import javafx.beans.property.DoubleProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * Objekt, který umožňuje kreslení v JavaFX
 */
public class CanvasPane extends Pane {
    private final Canvas canvas;

    private Color background = Color.WHITE;

    /**
     * Konstruktor
     */
    public CanvasPane() {
        canvas = new Canvas();

        this.getChildren().add(canvas);

        canvas.heightProperty().bind(this.heightProperty());
        canvas.widthProperty().bind(this.widthProperty());

        this.setOnMouseClicked(event -> this.requestFocus());
    }

    /**
     * Várít grafický kontext
     * @return grafický kontext
     */
    public GraphicsContext getGraphicsContext2D() {
        return canvas.getGraphicsContext2D();
    }

    /**
     * Vrátí vlastnost s šířkou plátna
     * @return vlastnost s šířkou plátna
     */
    public DoubleProperty getCanvasWidthProperty() {
        return canvas.widthProperty();
    }

    /**
     * Vrátí vlastnost s výškou plátna
     * @return vlastnost s výškou plátna
     */
    public DoubleProperty getCanvasHeightProperty() {
        return canvas.heightProperty();
    }

    /**
     * Vrátí velikost plátna
     * @return velikost plátna
     */
    public double getCanvasSize() { return this.canvas.getHeight(); }

    /**
     * vrátí barvu pozadí
     * @return barva pozadí
     */
    public Color getBackgroundColor() {
        return background;
    }
}
