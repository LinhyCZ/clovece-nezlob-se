package cz.linhy.clovece.customComponents;

/**
 * Přepravka obsahující souřadnice X a Y
 */
public class Point {
    private final double x;
    private final double y;

    /**
     * Konstruktor
     * @param x souřadnice X
     * @param y souřadnice Y
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Vrátí souřadnici X
     * @return souřadnice X
     */
    public double getX() {
        return x;
    }

    /**
     * Vrátí souřadnici Y
     * @return souřadnice Y
     */
    public double getY() {
        return y;
    }


    /**
     * Porovná, zda jsou dva body totožné
     * @param object objekt, který se porovnává
     * @return true = jsou totožné, false = nejsou totožné
     */
    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object instanceof Point) {
            Point p1 = (Point) object;
            return p1.getX() == this.getX() && p1.getY() == this.getY();
        }
        return false;
    }

    /**
     * Převede bod na řetězec
     * @return čitelný text obsahující informace o bodě
     */
    @Override
    public String toString() {
        return "[" + getX() + ":" + getY() + "]";
    }
}
