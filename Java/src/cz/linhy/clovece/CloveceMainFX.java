package cz.linhy.clovece;

import cz.linhy.clovece.game.Game;
import cz.linhy.clovece.windows.LobbyWindow;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Výchozí třída programu, spustí herní okno a okno hlavního lobby
 */
public class CloveceMainFX extends Application {

    /**
     * Spustí herní okno a okno hlavního lobb
     * @param primaryStage hlavní Stage
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setMinWidth(400);
        primaryStage.setMinHeight(400);
        primaryStage.setTitle("Hello world (:");

        BorderPane pane = new BorderPane();
        pane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

        Scene sc = new Scene(pane);
        primaryStage.setScene(sc);
        primaryStage.show();

        Game game = Game.createInstance(primaryStage);
        pane.setCenter(game.getGamePane());
        pane.setTop(game.createButtonBox());

        primaryStage.setOnCloseRequest((e) -> {
            Platform.exit();
        });

        LobbyWindow.createInstance(primaryStage).show();
    }


    /**
     * Vstupní bod programu
     * @param args argumenty z příkazu
     */
    public static void main(String[] args) {
        launch(args);
    }
}
