package cz.linhy.clovece.interfaces;

import cz.linhy.clovece.game.drawer.GameDrawer;

/**
 * Interface pro objekty, které lze vykreslit
 */
public interface IDrawable {
    /**
     * Slouží pro vykreslení daného objektu
     * @param gd kreslítko
     */
    public void draw(GameDrawer gd);
}
