package cz.linhy.clovece;

/**
 * Výchozí místo programu, slouží pouze pro oklamání JavaFX
 */
public class CloveceMain {
    /**
     * Vstupní bod programu
     * @param args argumenty
     */
    public static void main(String[] args) {
        CloveceMainFX.main(args);
    }
}
