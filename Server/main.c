#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/time.h>
#include "structures.h"
#include "player.h"
#include "game.h"
#include "list.h"
#include "logic.h"
#include "functions.h"
#include <unistd.h>

pthread_t thread_id;
struct timeval timeout;

list *game_list;

/* ____________________________________________________________________________

    void player_leave(int client_socket)

    Zpracuje hráče, který se odpojil, odstraní ho ze hry a uvolní paměť,
    Odešle zprávu o odpojení ostatním hráčům v lobby
   ____________________________________________________________________________
*/
void player_leave(int client_socket) {
    printf("INFO: Player is leaving! Socket: ");
    printf("%i \n", client_socket);
    player *leaving_player = find_player_by_socket(client_socket);
    if (leaving_player != NULL) {
        game *leaving_game = find_game_by_player(game_list, leaving_player);

        if (leaving_game != NULL) {
            if (leaving_game->state == LOBBY) {
                remove_player_from_game(game_list, leaving_game, leaving_player);

                char *buff = calloc(200, sizeof(char));
                char *players_left = get_players_in_game_string(leaving_game);
                sprintf(buff, "PU;%i;%s", leaving_game->available_colors, players_left);
                broadcast(leaving_game, buff);
                free(buff);
                free(players_left);
            } else {
                char* buffer = malloc(200);
                sprintf(buffer, "PL;%i;%s", leaving_player->color, leaving_player->name);
                broadcast(leaving_game, buffer);
                free(buffer);

                if (color_to_player_index(leaving_player->color) == leaving_game->current_player) {
                    next_player(leaving_game);
                    sem_post(&leaving_game->semaphore_game);
                }
            }
        }

        leaving_player->connected = 0;
    }
}

/* ____________________________________________________________________________

    int all_selected_color(game *gm)

    Ověří, zda všichni hráči v lobby již mají vybranou barvu. Vrátí 1 pokud ano,
    0 pokud ne.
   ____________________________________________________________________________
*/
int all_selected_color(game *gm) {
    int playersWithColor = 0;

    if (gm->player_blue != NULL) playersWithColor++;
    if (gm->player_green != NULL) playersWithColor++;
    if (gm->player_yellow != NULL) playersWithColor++;
    if (gm->player_red != NULL) playersWithColor++;

    if (gm->number_of_players == playersWithColor) {
        return 1;
    }

    return 0;
}

/* ____________________________________________________________________________

    void socket_error(int client_socket)

    Ukončí připojení socketu při nevalidním parametru.
   ____________________________________________________________________________
*/
void socket_error(int client_socket) {
    char *buffer = malloc(200);
    sprintf(buffer, "ERR");
    send_message(client_socket, buffer);
    free(buffer);

    shutdown(client_socket, SHUT_RDWR);
}

/* ____________________________________________________________________________

    int route_events(char message[], int client_socket)

    Rozdělí stavové zprávy a předá zprávy funkcím pro zpracování,
    vrátí 1 pokud se jedná o nevalidní zprávu
   ____________________________________________________________________________
*/
int route_events(char message[], int client_socket) {
    int i, j;
    printf("DEBUG: Route message: %s \n", message);

    char *command = strtok(message, ";");
    if (strcmp(command, "L") == 0) {
        char *playerName = strtok(NULL, ";");

        printf("INFO: Login from player with name: %s \n", playerName);

        player *plr = new_player(client_socket, playerName);
        add_player_to_list(plr);

        char* buffer = calloc(200, sizeof(char));
        sprintf(buffer, "LR;%i", plr->id);

        send_message(client_socket, buffer);
        free(buffer);
    } else if (strcmp(command, "N") == 0) {
        printf("INFO: Creating new game lobby.");

        char *player_char = strtok(NULL, ";");
        int player_ID = convert_int(player_char);

        if (player_ID == INT_MIN) {
            printf("ERROR: Could not convert %s to int, closing connection.\n", player_char);
        }

        char *lobby_ID = create_lobby_ID();

        while (game_exists(game_list, lobby_ID)) {
            free(lobby_ID);
            lobby_ID = create_lobby_ID(player_ID);
        }

        player *starting_player = find_player_by_id(player_ID);
        if (starting_player == NULL) {
            printf("ERROR: Player with ID: %i not found, closing connection.\n", player_ID);
            socket_error(client_socket);
            return 1;
        }

        game *new_lobby = create_new_game(starting_player, lobby_ID);

        add(game_list, new_lobby);

        char* buffer = calloc(200, 1);
        sprintf(buffer, "NR;%i;%s", new_lobby->available_colors, lobby_ID);
        send_message(client_socket, buffer);
        free(buffer);
    } else if (strcmp(command, "J") == 0) {
        char *player_char = strtok(NULL, ";");
        int player_ID = convert_int(player_char);

        if (player_ID == INT_MIN) {
            printf("ERROR: Could not convert %s to int, closing connection.\n", player_char);
            socket_error(client_socket);
            return 1;
        }

        char *lobby_ID = strtok(NULL, ";");

        game *lobby = find_game(game_list, lobby_ID);
        player *joining_player = find_player_by_id(player_ID);

        if (joining_player == NULL) {
            printf("ERROR: Player with ID %i not found! Closing connection..\n", player_ID);
            socket_error(client_socket);
            return 1;
        }

        if (lobby == NULL) {
            printf("WARNING: Lobby with ID %s not found!\n", lobby_ID);

            char *buffer = malloc(200);
            sprintf(buffer, "JR;0");
            send_message(client_socket, buffer);
            free(buffer);
            return 0;
        }

        if (lobby->state == LOBBY && lobby->number_of_players < 4) {
            lobby->player_in_game[lobby->number_of_players] = joining_player;
            lobby->number_of_players = lobby->number_of_players + 1;

            char *buff = calloc(200, sizeof(char));
            char *players_left = get_players_in_game_string(lobby);
            sprintf(buff, "PU;%i;%s", lobby->available_colors, players_left);
            broadcast(lobby, buff);
            free(buff);

            buff = calloc(200, sizeof(char));
            sprintf(buff, "JR;1;%i;%s;%s", lobby->available_colors, lobby->code_of_lobby, players_left);
            send_message(client_socket, buff);
            free(buff);
            free(players_left);
        } else {
            printf("WARNING: Game not in LOBBY mode");

            char *buffer = malloc(200);
            sprintf(buffer, "JR;0");
            send_message(client_socket, buffer);
            free(buffer);
        }
    } else if (strcmp(command, "COR") == 0){
        int player_ID = convert_int(strtok(NULL, ";"));
        if (player_ID == INT_MIN) {
            socket_error(client_socket);
            return 1;
        }

        int color = convert_int(strtok(NULL, ";"));
        if (color == INT_MIN) {
            socket_error(client_socket);
            return 1;
        }

        player *plr = find_player_by_id(player_ID);
        game *lobby = find_game_by_player(game_list, plr);
        lobby->available_colors -= color;

        set_color(lobby, plr, color);


        if (color == 16) {
            lobby->player_green = plr;
        } else if (color == 8) {
            lobby->player_blue = plr;
        } else if (color == 4) {
            lobby->player_yellow = plr;
        } else {
            lobby->player_red = plr;
        }

        char *buffer = malloc(200);
        char *players_left = get_players_in_game_string(lobby);
        sprintf(buffer, "PU;%i;%s", lobby->available_colors, players_left);
        broadcast(lobby, buffer);

        free(buffer);
        free(players_left);
    } else if (strcmp(command, "Q") == 0) {
        player_leave(client_socket);
        return 1;
    } else if (strcmp(command, "S") == 0) {
        char *game_code = strtok(NULL, ";");
        game *gm = find_game(game_list, game_code);

        if (gm == NULL) {
            printf("ERROR: Cannot start game with ID %s, not found, closing connection..\n", game_code);
            socket_error(client_socket);
            return 1;
        }

        if (all_selected_color(gm)) {
            gm->state = STARTED;
            srand(time(NULL));
            gm->current_player = rand() % 4;

            char *buffer = malloc(200);
            sprintf(buffer, "SR");
            broadcast(gm, buffer);
            free(buffer);

            thread_id = 0;
            pthread_create(&thread_id, NULL, start_game, (void *) gm);
        } else {
            char *buffer = malloc(200);
            sprintf(buffer, "SR;1");
            send_message(client_socket, buffer);
            free(buffer);
        }

    } else if (strcmp(command, "C") == 0) {
        char *game_code = strtok(NULL, ";");
        game *gm = find_game(game_list, game_code);
        player *plr = get_player_from_color_index(gm);

        if (gm == NULL) {
            printf("ERROR: Cannot throw cube in game with ID %s, not found, closing connection..\n", game_code);
            socket_error(client_socket);
            return 1;
        }

        throw_cube(gm);

        int can_move = can_player_move(gm, plr);

        char *buffer = malloc(200);
        sprintf(buffer, "CR;%i;%i;%i", plr->color, gm->dice, can_move);
        broadcast(gm, buffer);
        free(buffer);

        if (!can_move) {
            if (plr->had_three_throws == 0) {
                plr->number_of_throws = plr->number_of_throws + 1;

                if (plr->number_of_throws == 3) {
                    plr->number_of_throws = 0;
                    next_player(gm);
                }
            } else {
                next_player(gm);
            }
            sem_post(&(gm->semaphore_game));
        } else {
            plr->had_three_throws = 1;
        }
    } else if (strcmp(command, "M") == 0) {
        char *lobby_id = strtok(NULL, ";");
        char *player_char = strtok(NULL, ";");
        int player_id = convert_int(player_char);

        if (player_id == INT_MIN) {
            printf("ERROR: Could convert player ID from %s, closing connection.\n", player_char);
            socket_error(client_socket);
            return 1;
        }

        char *figure_index_char = strtok(NULL, ";");
        int figure_index = convert_int(figure_index_char);
        if (figure_index == INT_MIN) {
            printf("ERROR: Could convert figure index from %s, closing connection.\n", figure_index_char);
            socket_error(client_socket);
            return 1;
        }

        game *gm = find_game(game_list, lobby_id);
        player *p = find_player_by_id(player_id);

        if (gm == NULL) {
            socket_error(client_socket);
            printf("ERROR: Could find game with ID %s, closing connection.\n", lobby_id);
            return 1;
        }

        if (p == NULL) {
            socket_error(client_socket);
            printf("ERROR: Could find player with ID %i, closing connection.\n", player_id);
            return 1;
        }

        move_figure(gm, p, figure_index);
    } else if (strcmp("R", command) == 0) {
        char *player_char = strtok(NULL, ";");
        int player_ID = convert_int(player_char);
        if (player_ID == INT_MIN) {
            socket_error(client_socket);
            printf("ERROR: Could convert player ID from %s, closing connection.\n", player_char);
            return 1;
        }

        player *plr = find_player_by_id(player_ID);

        if (plr != NULL) {
            plr->connected = 1;
            plr->socket = client_socket;

            game *gm = find_game_by_player(game_list, plr);

            if (gm != NULL) {
                char *buffer = malloc(200);
                sprintf(buffer, "PRC;%i", plr->color);
                broadcast(gm, buffer);
                free(buffer);

                buffer = malloc(200);
                sprintf(buffer, "RR;");

                for (i = 0; i < gm->number_of_players; i++) {
                    player *p = gm->player_in_game[i];
                    sprintf(buffer + strlen(buffer), "%i;", p->color);

                    for (j = 0; j < 4; j++) {
                        sprintf(buffer + strlen(buffer), "%i;%i;", p->figures[j]->figure_index,
                                translate_figure_index_to_path(p->figures[j]->index_on_board, p->figures[j]->color));
                    }
                }

                buffer[strlen(buffer)] = 0;
                send_message(plr->socket, buffer);
                free(buffer);

                if (color_to_player_index(plr->color) == gm->current_player) {
                    next_player(gm);
                    sem_post(&gm->semaphore_game);
                }
            } else {
                char *buffer = malloc(200);
                sprintf(buffer, "RR;0");
                send_message(plr->socket, buffer);
                free(buffer);
            }
        } else {
            char *buffer = malloc(200);
            sprintf(buffer, "RR;0");
            send_message(client_socket, buffer);
            free(buffer);
        }
    } else if (strcmp("PING", command) == 0) {
        player *plr = find_player_by_socket(client_socket);
        char *buffer = malloc(200);

        if (plr != NULL) {
            game *gm = find_game_by_player(game_list, plr);

            if (gm != NULL) {
                if (gm->last_broadcast != NULL && gm->state == STARTED) {
                    sprintf(buffer, "PONG;%s", gm->last_broadcast);
                } else {
                    sprintf(buffer, "PONG");
                }
            } else {
                sprintf(buffer, "PONG");
            }
        } else {
            sprintf(buffer, "PONG");
        }

        send_message(client_socket, buffer);
        free(buffer);
    } else {
        printf("ERROR: Unknown command: %s \n", command);
        shutdown(client_socket, SHUT_RDWR);
        close(client_socket);
        return 1;
    }

    return 0;
}


/* ____________________________________________________________________________

    char *removeNL (char * message)

    Odstraní z řetězce znaky \n\r
   ____________________________________________________________________________
*/
char *removeNL (char * message) {
    int i, e, index = 0;
    int len = strlen(message);

    char *new_message = calloc(len, sizeof(char));
    for (i = 0; i < len; i++) {
        e = message[i];

        if (e != 10 && e != 13) {
            new_message[index] = message[i];
            index++;
        }
    }

    new_message[index] = '\0';
    return new_message;
}

/* ____________________________________________________________________________

    void * process_message(void * arg)

    Obsahuje smyčku, ve kterých přijíma zprávy a dále je předává funkci
    route_message k dalšímu zpracování
   ____________________________________________________________________________
*/
void * process_message(void * arg) {
    int received_code;
    int client_socket = *(int *) arg;
    char *messageBuffer = calloc(100, sizeof(char));

    setsockopt (client_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
    while ((received_code = recv(client_socket, messageBuffer, 100 * sizeof(char), 0)) != 0) {
        char *message = removeNL(messageBuffer);

        if (received_code == -1) {
            printf("ERROR: Failed receiving message! Message: %s \n", strerror(errno));
        } else {
            if (!strlen(message)) {
                printf("DEBUG: Received only new line, skipping.\n");
            } else {
                if (route_events(message, client_socket)) {
                  break;
                };
            }
        }
        memset(messageBuffer, 0, 100 * sizeof(char));
        free(message);
    }

    player_leave(client_socket);
    free(arg);
    free(messageBuffer);

    return NULL;
}

/* ____________________________________________________________________________

    void * start_server(void * arg)

    Funkce připraví a spustí socket server.
   ____________________________________________________________________________
*/
void * start_server(void * arg) {
    printf("INFO: Starting server.\n");
    struct server_start_struct *params = arg;

    struct sockaddr_in local_address = {0};
    struct sockaddr_in remote_address = {0};

    socklen_t remote_address_length;

    int server_socket = socket(AF_INET, SOCK_STREAM, 0);

    int flag = 1;
    setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(int));

    timeout.tv_sec = 30;

    if (server_socket <= 0) {
        printf("FATAL: Cannot open socket! Message: %s \n", strerror(errno));
        exit(1);
    }

    /* Erase to prevent other bits.. #YouHaveToLoveC :) */
    memset(&local_address, 0, sizeof(struct sockaddr_in));

    local_address.sin_family = AF_INET;
    local_address.sin_addr = params->ip;
    local_address.sin_port = htons(params->port);

    int bind_return = bind(server_socket, (const struct sockaddr *) &local_address, sizeof(struct sockaddr_in));

    if (!bind_return) {
        printf("INFO: Bind successful!\n");
    } else {
        printf("FATAL: Cannot bind socket! Message: %s \n", strerror(errno));
        exit(1);
    }

    int listen_return = listen(server_socket, 20);

    if (!listen_return) {
        printf("INFO: Listen successful!\n");
    } else {
        printf("FATAL: Cannot listen! Message: %s \n", strerror(errno));
        exit(1);
    }

    while(true) {
        int client_socket = accept(server_socket, (struct sockaddr *) &remote_address, &remote_address_length);

        if (client_socket > 0) {
            int* thread_socket = calloc(1, sizeof(int));
            *thread_socket = client_socket;

            thread_id = 0;
            pthread_create(&thread_id, NULL, (void *) &process_message, (void *) thread_socket);
        } else {
            printf("FATAL: Cannot process client socket! Message: %s \n", strerror(errno));
            exit(1);
        }
    }
}

/* ____________________________________________________________________________

    void free_memory()

    Uvolní paměť po serveru.
   ____________________________________________________________________________
*/
void free_memory() {
    free_list(game_list);
}

struct server_start_struct parse_params(int argc, char const *argv[]) {
    int i;
    int readingMode = -1; /* -1 = not set; 0 = reading IP; 1 = reading port */
    int ip_initialized = 0;

    struct in_addr ip = {0};
    int port = -1;

    for (i = 1; i < argc; i++) {
        if (readingMode == 0) { /*Reading IP */
            if (strcmp(argv[i], "A") == 0) {
                ip.s_addr = htonl(INADDR_ANY);

                ip_initialized = 1;
                readingMode = -1;
            } else {
                int result = inet_aton(argv[i], &ip);
                if (result == 0) {
                    printf("WARNING: Wrong address passed in parameter: %s, using default 127.0.0.1 \n", argv[i]);
                    inet_aton("127.0.0.1", &ip);
                }
                ip_initialized = 1;
                readingMode = -1;
            }
        } else if (readingMode == 1) { /* Reading port*/
            int convertedPort = convert_int((char *) argv[i]);

            if (convertedPort > 0 && convertedPort < 65535) {
                port = convertedPort;
            } else {
                printf("WARNING: Wrong port passed in parameter: %s, using default 9652 \n", argv[i]);
            }
            readingMode = -1;
        } else if (strcmp(argv[i], "-i") == 0) {
            readingMode = 0;
        } else if (strcmp(argv[i], "-p") == 0) {
            readingMode = 1;
        }
    }

    if (!ip_initialized) {
        inet_aton("127.0.0.1", &ip);
    }

    if (port == -1) {
        port = 9652;
    }

    struct server_start_struct params;
    params.ip = ip;
    params.port = port;

    return params;
};

/* ____________________________________________________________________________

    int main(int argc, char const *argv[])

    Vstupní bod programu, zpracuje a ověří zadané parametry a předá je
    pro nastavení a spuštění serveru.
   ____________________________________________________________________________
*/
int main(int argc, char const *argv[]) {
    game_list = create_list();

    struct server_start_struct params = parse_params(argc, argv);
    thread_id = 0;

    pthread_create(&thread_id, NULL, start_server, (void *) &params);

    char vstup[1024];
    while(scanf("%s", vstup) != -1) {
        if(strcmp(vstup, "q") == 0) {
            break;
        } else if(strcmp(vstup, "p") == 0) {
            printf("INFO: Connected players: \n");
            print_players();
        } else if(strcmp(vstup, "g") == 0) {
            printf("INFO: Started games: \n");
            print_games(game_list);
        }
    }

    free_memory();
    pthread_cancel(thread_id);
    return 0;
}
