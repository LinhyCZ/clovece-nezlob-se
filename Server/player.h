/*
 *
 */

#ifndef SERVER_PLAYER_H
#define SERVER_PLAYER_H

#include <stdio.h>
#include <stdlib.h>
#include "figure.h"

/* ____________________________________________________________________________

    typedef struct PLAYER

    Struktura obsahující veškeré informace o hrčí, jako například jeho socket,
    ID, barvu, jméno, zda je připojen a další.
   ____________________________________________________________________________
*/
typedef struct PLAYER {
    int socket;
    int id;
    int color;
    int had_three_throws;
    int number_of_throws;
    int connected;
    char* name;
    struct figure *figures[4];
    struct PLAYER *next;
} player;

void add_player_to_list(player *plr);
void print_players();
void remove_player_from_list_id(int id);
player *new_player(int socket, char *jmeno);
player *find_player_by_id(int player_id);
player *find_player_by_socket(int player_socket);

#endif