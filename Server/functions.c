/*
 *
 */

#include "functions.h"

int path_red[] = {0, 1, 2, 3, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59};
int path_yellow[] = {4, 5, 6, 7, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 60, 61, 62, 63};
int path_blue[] = {8, 9, 10, 11, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 64, 65, 66, 67};
int path_green[] = {12, 13, 14, 15, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 68, 69, 70, 71};

/* ____________________________________________________________________________

    void send_message(int socket, char *message)

    Odešle danou zprávu na zadaný Socket.
   ____________________________________________________________________________
*/
void send_message(int socket, char *message) {
    message[strlen(message)] = 7;
    send(socket, message, strlen(message), 0);
}


/* ____________________________________________________________________________

    void broadcast(game *lobby, char *message)

    Odešle zprávu všem hráčům v zadaném Lobby.
   ____________________________________________________________________________
*/
void broadcast(game *lobby, char *message) {
    int i;

    for (i = 0; i < 4; i++) {
        if (!lobby->player_in_game[i]) {
            break;
        }

        if (lobby->player_in_game[i]->connected == 0) {
            continue;
        }

        send_message(lobby->player_in_game[i]->socket, message);
    }

    free(lobby->last_broadcast);
    lobby->last_broadcast = malloc(strlen(message));
    strcpy(lobby->last_broadcast, message);
}


/* ____________________________________________________________________________

    int convert_int(char *str)

    Převede znakový řetězec na číslo typu int.
   ____________________________________________________________________________
*/
int convert_int(char *str) {
    errno = 0;
    char *end;
    int value = strtol(str, &end, 10);

    if (errno != 0) {
        printf("ERROR: Error converting string '%s' into number! Message: %s \n", str, strerror(errno));
        return INT_MIN;
    }

    return value;
}

/* ____________________________________________________________________________

    char* create_lobby_ID()

    Vygeneruje náhodný řetězec s délkou 5 znaků, který představuje ID herního
    lobby.
   ____________________________________________________________________________
*/
char* create_lobby_ID() {
    int i;

    static char charset[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    char *randomString = malloc(ID_LENGTH + 1);

    if (randomString) {
        srand(time(NULL));
        for (i = 0; i < ID_LENGTH; i++) {
            int key = rand() % (int) (sizeof(charset) -1);
            randomString[i] = charset[key];
        }

        randomString[ID_LENGTH] = '\0';
    }

    return randomString;
}

/* ____________________________________________________________________________

    int translate_figure_index_to_path(int path, int color)

    Převede pozici figurky na číslo odpovídající políčku na herním poli.
   ____________________________________________________________________________
*/
int translate_figure_index_to_path(int path, int color) {
    if (path >= 48) {
        return -1;
    }

    if (color == 2) {
        return path_red[path];
    } else if (color == 4) {
        return path_yellow[path];
    } else if (color == 8) {
        return path_blue[path];
    } else {
        return path_green[path];
    }
}

/* ____________________________________________________________________________

    int color_to_player_index(int c)

    Převede číslo barvy na index používaný ve struktuře game.
   ____________________________________________________________________________
*/
int color_to_player_index(int c) {
    if (c == 2) {
        return 0;
    } else if (c == 4) {
        return 1;
    } else if (c == 8) {
        return 2;
    } else {
        return 3;
    }
}

/* ____________________________________________________________________________

    void set_color(game *gm, player *p, int color)

    Nastaví barvu hráči a přidá mu figurky dané barvy.
   ____________________________________________________________________________
*/
void set_color(game *gm, player *p, int color) {
    int i;
    p->color = color;

    for (i = 0; i < 4; i++) {
        p->figures[i] = calloc(1, sizeof(struct figure));
        p->figures[i]->color = p->color;
        p->figures[i]->figure_index = i;
        p->figures[i]->index_on_board = i;

        int new_point = translate_figure_index_to_path(i, color);
        gm->figure_points[new_point] = p->figures[i];

        p->figures[i]->sf = NOT_STARTED;
    }
}