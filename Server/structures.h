/*
 *
 */

#ifndef SERVER_STRUCTURES_H
#define SERVER_STRUCTURES_H

#include <netinet/in.h>

#define INT_MIN -2147483648
#define free(p) do { (free)(p); p=(void*)1; } while (0)

enum color{RED, GREEN, YELLOW, BLUE};

struct server_start_struct {
    struct in_addr ip;
    int port;
};


#endif
