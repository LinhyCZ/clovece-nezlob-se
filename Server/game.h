/*
 *
 */

#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "player.h"
#include "list.h"

#ifndef SERVER_GAME_H
#define SERVER_GAME_H

enum state_of_game{LOBBY, STARTED, ENDED};

/* ____________________________________________________________________________

    typedef struct GAME

    Obsahuje veškeré informace o hře, včetně seznamu hráčů, aktuálního stavu hry,
    stavu kostky a dalších
   ____________________________________________________________________________
*/
typedef struct GAME {
    char* code_of_lobby;
    int number_of_players;
    player *player_in_game[4];
    int current_player;
    int dice;
    enum state_of_game state;
    int available_colors;

    char *last_broadcast;
    struct figure *figure_points[72];

    sem_t semaphore_game;

    player *player_red;
    player *player_yellow;
    player *player_blue;
    player *player_green;
} game;

void print_games(list *game_list);
void remove_game_from_list(list* game_list, char* code);
game *create_new_game(player* starting_player, char* code_of_game);
int game_exists(list* game_list, char* code);
game *find_game(list *game_list, char* code);
char *get_players_in_game_string(game *gm);
int remove_player_from_game(list *game_list, game *gm, player *plr);
game *find_game_by_player(list *game_list, player* plr);


#endif
