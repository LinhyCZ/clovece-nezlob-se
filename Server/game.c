/*
 *
 */

#include "game.h"

/* ____________________________________________________________________________

    game *create_new_game(player* starting_player, char* code_of_game)

    Alokuje paměť novému lobby a nastaví výchozí hodnoty
   ____________________________________________________________________________
*/
game *create_new_game(player* starting_player, char* code_of_game) {
    game* new_game = calloc(1, sizeof(game));
    new_game->player_in_game[0] = starting_player;
    new_game->player_in_game[1] = NULL;
    new_game->player_in_game[2] = NULL;
    new_game->player_in_game[3] = NULL;

    new_game->available_colors = 30;

    new_game->number_of_players = 1;
    new_game->code_of_lobby = malloc(5);
    sprintf(new_game->code_of_lobby, "%s", code_of_game);

    new_game->figure_points[0] = calloc(72, sizeof(struct figure));

    new_game->state = LOBBY;
    new_game->dice = 0;

    sem_init(&(new_game->semaphore_game), 0, 0);

    return new_game;
}

/* ____________________________________________________________________________

    game *find_game(list *game_list, char* code)

    Najde hru v seznamu podle zadaného kódu.
   ____________________________________________________________________________
*/
game *find_game(list *game_list, char* code) {
    int i;
    for (i = 0; i < count(game_list); i++) {
        game *current = get(game_list, i);
        if (strcmp(current->code_of_lobby, code) == 0) {
            return current;
        }
    }

    return NULL;
}

/* ____________________________________________________________________________

    game *find_game_by_player(list *game_list, player* plr)

    Najde hru, ve které se nachází specifikovaný hráč. Vrátí null, pokud takovou hru nenajde
   ____________________________________________________________________________
*/
game *find_game_by_player(list *game_list, player* plr) {
    int i, j;
    for (i = 0; i < count(game_list); i++) {
        game *current = get(game_list, i);
        for (j = 0; j < current->number_of_players; j++) {
            if(current->player_in_game[j]->id == plr->id) {
                return current;
            }
        }
    }

    return NULL;
}

/* ____________________________________________________________________________

    void free_game(game *gm)

    Uvolní paměť po hře
   ____________________________________________________________________________
*/
void free_game(game *gm) {
    int i;
    for (i = 0; i < 4; i++) {
        free(gm->player_in_game[i]);
    }
    for (i = 0; i < 72; i++) {
        free(gm->figure_points[i]);
    }
    free(gm->code_of_lobby);
    free(gm);
}

/* ____________________________________________________________________________

    int game_exists(list* game_list, char* code) {

    Ověří, zda existuje hra se zadaným kódem lobby
   ____________________________________________________________________________
*/
int game_exists(list* game_list, char* code) {
    int i;
    for (i = 0; i < count(game_list); i++) {
        game *current = get(game_list, i);
        if (strcmp(current->code_of_lobby, code) == 0) {
            return 1;
        }
    }

    return 0;
}


/* ____________________________________________________________________________

    char *get_players_in_game_string(game *gm)

    Vrátí řetězec obsahující všechny hráče ve hře - jejich jméno a číslo barvy
   ____________________________________________________________________________
*/
char *get_players_in_game_string(game *gm) {
    int i;
    char* buffer = malloc(150);

    for (i = 0; i < gm->number_of_players; i++) {
        if (!gm->player_in_game[i]) {
            break;
        }

        sprintf(buffer + strlen(buffer), "%s-%i", gm->player_in_game[i]->name, gm->player_in_game[i]->color);
        if(i != gm->number_of_players - 1) {
            sprintf(buffer + strlen(buffer),",");
        }
    }

    return buffer;
}

/* ____________________________________________________________________________

    int remove_player_from_game(list *game_list, game *gm, player *plr)

    Odstaní hráče z dané hry. Pokud je počet hráčů 0, odstraní dané lobby.
   ____________________________________________________________________________
*/
int remove_player_from_game(list *game_list, game *gm, player *plr) {
    int i;
    int newIndex = 0;
    for (i = 0; i < gm->number_of_players; i++) {
        if (gm->player_in_game[i]->id == plr->id) {
            continue;
        } else {
            if (gm->player_in_game[i] == NULL) {
                break;
            }
            gm->player_in_game[newIndex] = gm->player_in_game[i];
            newIndex++;
        }
    }

    gm->player_in_game[newIndex] = NULL;
    gm->number_of_players = gm->number_of_players - 1;

    if (plr->color == 2) {
        gm->player_red = NULL;
    } else if (plr->color == 4) {
        gm->player_yellow = NULL;
    } else if (plr->color == 8) {
        gm->player_blue = NULL;
    } else {
        gm->player_green = NULL;
    }

    gm->available_colors += plr->color;

    if (gm->number_of_players == 0) {
        int index = index_of(game_list, gm->code_of_lobby);
        free_game(gm);
        remove_from_list(game_list, index);

        return 0;
    }

    return 1;
}

/* ____________________________________________________________________________

    void print_games(list *game_list)

    Vypíše informace o existujících hrách
   ____________________________________________________________________________
*/
void print_games(list *game_list) {
    int i;
    for (i = 0; i < count(game_list); i++) {
        game *current = get(game_list, i);

        if (current->state == STARTED || current->state == LOBBY) {
            printf("%s", current->code_of_lobby);
            printf(": ");

            char *players_string = get_players_in_game_string(current);
            printf("%s", players_string);
            free(players_string);

            printf("\n");
        }
    }
    printf("\n");
}
