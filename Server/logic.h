/*
 *
 */

#ifndef SERVER_LOGIC_H
#define SERVER_LOGIC_H

#include "game.h"
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <time.h>
#include "structures.h"
#include "functions.h"
#include "figure.h"

int translate_figure_index_to_path(int path, int color);
int can_player_move(game *gm, player *p);
void *start_game(void *arg);
void throw_cube(game *gm);
void next_player(game *gm);
player *get_player_from_color_index(game *this_game);
void move_figure(game *gm, player *p, int figure_index);

#endif
