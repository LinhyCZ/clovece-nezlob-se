/*
 *
 */

#ifndef SERVER_FIGURE_H
#define SERVER_FIGURE_H

enum state_of_figure {NOT_STARTED, ON_BOARD, SAFE};

/* ____________________________________________________________________________

    struct figure

    Obsahuje informace o figurce, jako například index figurky, barvu anebo
    pozici.
   ____________________________________________________________________________
*/
struct figure {
    int figure_index;
    int index_on_board;
    int color;
    enum state_of_figure sf;
};

#endif
