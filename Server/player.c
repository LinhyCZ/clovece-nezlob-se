/*
 *
 */
#include "player.h"

player *first_player = NULL;
int player_counter = 0;

/* ____________________________________________________________________________

    void add_player_to_list(player *plr)

    Přidá hráče do spojového seznamu.
   ____________________________________________________________________________
*/
void add_player_to_list(player *plr) {
    if (!plr) {return;}

    if (first_player == NULL) {
        first_player = plr;
    } else {
        plr->next = first_player;
        first_player = plr;
    }
}

/* ____________________________________________________________________________

    void free_player(player *pPlayer)

    Uvolní paměť po hráči.
   ____________________________________________________________________________
*/
void free_player(player *pPlayer) {
    int i;
    free(pPlayer->name);
    for (i = 0; i < 4; i++) {
        free(pPlayer->figures[i]);
    }
    free(pPlayer);
}

/* ____________________________________________________________________________

    void remove_player_from_list_id(int id)

    Odstraní hráče se zadaným ID ze seznamu.
   ____________________________________________________________________________
*/
void remove_player_from_list_id(int id) {
    if (!first_player) { return; }
    player *current = first_player;
    player *previous = NULL;

    do {
        if (current->id == id) {
            if (previous == NULL) {
                first_player = first_player->next;
                free_player(current);
                current = NULL;
            } else {
                previous->next = current->next;
                free_player(current);
                current = NULL;
            }

        } else {
            previous = current;
            current = current->next;
        }
    } while (current != NULL);
}

/* ____________________________________________________________________________

    player *find_player_by_id(int player_id)

    Najde hráče v seznamu podle jeho ID.
   ____________________________________________________________________________
*/
player *find_player_by_id(int player_id) {
    player *current = first_player;

    while (current != NULL) {
        if (current->id == player_id) {
            return current;
        }

        current = current->next;
    }

    return NULL;
}

/* ____________________________________________________________________________

    player *find_player_by_socket(int player_socket)

    Najde hráče v seznamu podle jeho socketu.
   ____________________________________________________________________________
*/
player *find_player_by_socket(int player_socket) {
    player *current = first_player;

    while (current != NULL) {
        if (current->socket == player_socket) {
            return current;
        }

        current = current->next;
    }

    return NULL;
}

/* ____________________________________________________________________________

    player *new_player(int socket, char *jmeno)

    Alokuje paměť novému hráči a nastaví výchozí hodnoty
   ____________________________________________________________________________
*/
player *new_player(int socket, char *jmeno) {
    player *plr = malloc(sizeof(player));
    if (!plr) {
        printf("FATAL: Could not assign memory");
        exit(1);
    }

    plr->name = malloc(25);
    plr->connected = 1;
    plr->color = 0;
    plr->had_three_throws = 0;
    plr->number_of_throws = 0;
    sprintf(plr->name, "%s", jmeno);

    plr->socket = socket;
    plr->id = player_counter;
    player_counter++;

    return plr;
}

/* ____________________________________________________________________________

    void print_players()

    Vypíše informace o existujících hráčích
   ____________________________________________________________________________
*/
void print_players() {
    player *current = first_player;

    while (current != NULL) {
        printf("%s", current->name);
        printf(", ");
        current = current->next;
    }

    printf("\n");
}