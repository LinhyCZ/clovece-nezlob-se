# cmake_minimum_required(VERSION <specify CMake version here>)
project(Server C)

SET(CMAKE_CXX_FLAGS -pthread)
set(CMAKE_C_STANDARD 11)

add_executable(Server figure.h main.c structures.h player.c player.h game.c game.h list.c list.h node.h logic.h logic.c functions.c functions.h)
target_link_libraries(${PROJECT_NAME} pthread)