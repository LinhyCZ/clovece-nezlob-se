/*
 *
 */

#ifndef SERVER_FUNCTIONS_H
#define SERVER_FUNCTIONS_H

#include "game.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <errno.h>
#include "logic.h"
#include "figure.h"


#define ID_LENGTH 5

void send_message(int socket, char *message);
void broadcast(game *lobby, char *message);
char* create_lobby_ID();
void set_color(game *gm, player *p, int color);
int convert_int(char *str);
int color_to_player_index(int c);

#endif
