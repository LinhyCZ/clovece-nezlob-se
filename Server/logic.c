/*
 *
 */
#include "logic.h"

/* ____________________________________________________________________________

    void next_player(game *gm)

    Provede posun na dalšího hráče v pořadí v zadané hře.
   ____________________________________________________________________________
*/
void next_player(game *gm) {
    if (gm->state == STARTED) {
        if (gm->current_player == 3) {
            gm->current_player = -1;
        }

        gm->current_player = gm->current_player + 1;
        while (get_player_from_color_index(gm) == NULL) {
            if (gm->current_player == 3) {
                gm->current_player = -1;
            }
            gm->current_player = gm->current_player + 1;
        }

        if (get_player_from_color_index(gm)->connected == 0) {
            next_player(gm);
        }
    }
}

/* ____________________________________________________________________________

    void throw_cube(game *gm)

    Nastaví kostku v dané hře na náhodné číslo.
   ____________________________________________________________________________
*/
void throw_cube(game *gm) {
    srand(time(NULL));
    int number = (rand() % 6) + 1;

    gm->dice = number;
}

/* ____________________________________________________________________________

    player *get_player_from_color_index(game *this_game)

    Vrátí aktuálního hráče z dané hry podle jeho barvy.
   ____________________________________________________________________________
*/
player *get_player_from_color_index(game *this_game) {
    if (this_game->current_player == 0) {
        return this_game->player_red;
    } else if (this_game->current_player == 1) {
        return this_game->player_yellow;
    } else if (this_game->current_player == 2) {
        return this_game->player_blue;
    } else {
        return this_game->player_green;
    }
}

/* ____________________________________________________________________________

    int has_move_option(game *gm, struct figure *f)

    Ověří, zda hráč může pohnout s danou figurkou. Vrátí 1 pokud ano, 0 pokud ne.
   ____________________________________________________________________________
*/
int has_move_option(game *gm, struct figure *f) {

    int dice_number = gm->dice;
    int new_point;

    if (f->sf == SAFE) {
        return 0;
    }

    if (f->sf == NOT_STARTED && dice_number != 6) {
        return 0;
    } else if (f->sf == NOT_STARTED && dice_number == 6) {
        new_point = translate_figure_index_to_path(4, f->color);
    } else {
        new_point = translate_figure_index_to_path(f->index_on_board + dice_number, f->color);
    }


    if (new_point == -1) {
        return 0;
    }

    if (gm->figure_points[new_point] == NULL) {
        return 1;
    }

    if (gm->figure_points[new_point]->color == f->color) {
        return 0;
    }

    return 1;
}

/* ____________________________________________________________________________

    int can_player_move(game *gm, player* p)

    Ověří, zda hráč může pohnout alespoň s jednou jeho figurkou.
    Vrátí 1 pokud ano, 0 pokud ne.
   ____________________________________________________________________________
*/
int can_player_move(game *gm, player* p) {
    int i;
    for (i = 0; i < 4; i++) {
        if (has_move_option(gm, p->figures[i]) == 1) {
            return 1;
        }
    }

    return 0;
}

/* ____________________________________________________________________________

    int checkWin(player *p)

    Ověří, zda daný hráč nevyhrál hrz. Vrátí 1 pokud ano, 0 pokud ne.
   ____________________________________________________________________________
*/
int checkWin(player *p) {
    int i;
    for (i = 0; i < 4; i++) {
        if (p->figures[i]->sf != SAFE) {
            return 0;
        }
    }

    return 1;
}

/* ____________________________________________________________________________

    void move_figure(game *gm, player *p, int figure_index)

    Posune figurku s daným indexem o počet políček, který je spcifikovaný
    v proměnné uvnitř struktury game. O posunu informuje všechny hráče ve hře.
   ____________________________________________________________________________
*/
void move_figure(game *gm, player *p, int figure_index) {
    if (has_move_option(gm, p->figures[figure_index])) {
        int new_position, old_position;
        if (p->figures[figure_index]->sf == NOT_STARTED) {
            new_position = translate_figure_index_to_path(4, p->color);
            old_position = translate_figure_index_to_path(p->figures[figure_index]->index_on_board, p->color);
            p->figures[figure_index]->index_on_board = 4;
            p->figures[figure_index]->sf = ON_BOARD;
        } else {
            new_position = translate_figure_index_to_path(p->figures[figure_index]->index_on_board + gm->dice, p->color);
            old_position = translate_figure_index_to_path(p->figures[figure_index]->index_on_board, p->color);
            p->figures[figure_index]->index_on_board = p->figures[figure_index]->index_on_board + gm->dice;
        }

        if (p->figures[figure_index]->index_on_board >= 44) {
            p->figures[figure_index]->sf = SAFE;
        }

        char* buff = calloc(200, sizeof(char));
        sprintf(buff, "MR;%i;%i;%i", p->color, figure_index, new_position);

        if (gm->figure_points[new_position] != NULL) {
            int home_index = translate_figure_index_to_path(gm->figure_points[new_position]->figure_index, gm->figure_points[new_position]->color);
            gm->figure_points[new_position]->sf = NOT_STARTED;
            gm->figure_points[new_position]->index_on_board = gm->figure_points[new_position]->figure_index;
            gm->figure_points[home_index] = gm->figure_points[new_position];
            sprintf(buff + strlen(buff), ";%i;%i;%i", gm->figure_points[new_position]->color, gm->figure_points[new_position]->figure_index, home_index);
        }
        broadcast(gm, buff);
        free(buff);

        gm->figure_points[old_position] = NULL;
        gm->figure_points[new_position] = p->figures[figure_index];

        if (checkWin(p)) {
            char *buffer = malloc(200);
            sprintf(buffer, "E;%s;%i", p->name, p->color);
            broadcast(gm, buffer);
            free(buffer);

            gm->state = ENDED;
            printf("INFO: Game with ID %s ended! Winner is %s, congratulations!\n", gm->code_of_lobby, p->name);
        } else {
            if (gm->dice != 6) {
                next_player(gm);
            }

            sem_post(&(gm->semaphore_game));
        }
    } else {
        char *buffer = malloc(200);
        sprintf(buffer, "CR;%i;%i;1", p->color, gm->dice);
        send_message(p->socket, buffer);
        free(buffer);
    }
}

/* ____________________________________________________________________________

    void *start_game(void* arg)

    Spustí zadanou hru.
   ____________________________________________________________________________
*/
void *start_game(void* arg) {
    if (!arg) {
        return NULL;
    }

    game* this_game = arg;
    player *plr = get_player_from_color_index(this_game);

    while (plr == NULL) {
        this_game->current_player = rand() % 4;
        plr = get_player_from_color_index(this_game);
    }


    while (1) {
        if (this_game->state == ENDED) {
            break;
        }

        do {
            this_game->dice = 0;
            plr = get_player_from_color_index(this_game);

            char* buff = calloc(200, sizeof(char));
            sprintf(buff, "CA;%i", plr->color);
            broadcast(this_game, buff);
            free(buff);

            sem_wait(&(this_game->semaphore_game));
        } while (this_game->dice == 6);
    }

    return NULL;
}
